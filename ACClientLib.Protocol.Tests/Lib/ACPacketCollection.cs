using NUnit.Framework;
using System.IO;
using System.Threading.Tasks;
using System;
using ACClientLib.Protocol.Lib;

namespace ACClientLib.Protocol.Tests.Lib {
    public class ACPacketCollectionTests {
        private string _dataDir;

        [SetUp]
        public void Setup() {
            _dataDir = Path.Combine(Path.GetDirectoryName(GetType().Assembly.Location)!, "Data");
        }

        [Test]
        public async Task CanParseAllWithoutErrors() {
            foreach (var file in Directory.GetFiles(_dataDir, "*.pcap")) {
                if (!file.Contains("ace-login"))
                    continue;
                Console.WriteLine($"Parse: {file}");
                using var stream = File.OpenRead(Path.Combine(_dataDir, file));
                var pc = await ACPacketCollection.FromPCap(stream);

                Assert.That(pc, Is.Not.Null, $"{file} failed to parse (was null)");
                Assert.That(pc.Packets, Is.Not.Empty, $"{file} failed to parse any packets");

                /*
                var i = 0;
                foreach (var p in pc.Packets) {
                    Console.WriteLine($"Packet ({p.ACPacket.Direction}): #{i++} {p.ACPacket.Flags}");
                    Console.WriteLine(p.ACPacket.ParseInfo?.ToString().TrimEnd() ?? "No parse info...");
                    if (p.ACPacket.Messages is not null) {
                        Console.WriteLine("\tMessages:");
                        foreach (var msg in p.ACPacket.Messages) {
                            Console.WriteLine(msg.ParseInfo?.ToString(2).ToString() ?? "No parse info...");
                        }
                    }
                }
                */
            }
        }
    }
}