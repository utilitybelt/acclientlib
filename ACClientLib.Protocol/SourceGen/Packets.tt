﻿<#@ assembly name="netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51" #>
<#@ assembly name="System.Xml.Linq, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" #>
<#@ template debug="false" hostspecific="true" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Xml.XPath" #>
<#@ output extension=".cs" #>
<#@ include file="../../Shared/ProtocolHelpers.ttinclude" #>
<#@ include file="../../Shared/MultipleOutputHelper.ttinclude" #>
<#
    var manager = Manager.Create(Host, GenerationEnvironment);
    string protocolPath = Path.Combine(Path.GetDirectoryName(this.Host.TemplateFile), "..", "..", "Shared", "protocol.xml");
    LoadProtocol(protocolPath);

    var outputPath = Path.Combine(Path.GetDirectoryName(this.Host.TemplateFile), "..", "Generated", "Packets");
    if (!Directory.Exists(outputPath)) {
        Directory.CreateDirectory(outputPath);
    }

    foreach (var type in ProtocolXml.XPathSelectElements("/packets/*")) {
        var name = type.Attribute("name").Value;

        manager.StartNewFile(Path.Combine(outputPath, $"{name}.cs"));
        WriteAutoGenHeader("Lib/SourceGen/Packets.tt");
        
        WriteLine($"using System;");
        WriteLine($"using System.IO;");
        WriteLine($"using System.ComponentModel;");
        WriteLine($"using System.Collections.Generic;");
        WriteLine($"using System.Numerics;");
        WriteLine($"using ACClientLib.Enums;");
        WriteLine($"using ACClientLib.Protocol.Lib;");
        WriteLine($"using ACClientLib.Protocol.Types;");
        WriteLine($"using ACClientLib.Lib.Attributes;");
        WriteLine("namespace ACClientLib.Protocol.Packets {");
        PushIndent("\t");

        WriteSummaryComment(type.Attribute("text")?.Value);
        
        WriteLine($"public partial class {name} : IACPacket, IACDataType {{");
        PushIndent("\t");
        
        // fields
        WriteLine("public ParsedFieldInfo? ParseInfo { get; set; }");
        WriteLine("");
        WriteSummaryComment("The direction of this packet. Either server to client, or client to server.");
        WriteLine($"public PacketDirection Direction => PacketDirection.{name.Replace("Packet", "")};");
        WriteLine("");
        WriteSummaryComment("A list of any messages that got finished building from fragment data after this packet was received.");
        WriteLine($"public List<IACMessage>? Messages {{ get; set; }}");
        WriteLine("");

        var writtenFields = new List<string>();
        var acType = PacketTypes[name];
        foreach (var el in type.XPathSelectElements(".//field|.//vector|.//table|.//subfield")) {
            var fieldName = el.Attribute("name")?.Value;
            if (!string.IsNullOrEmpty(fieldName) && !writtenFields.Contains(fieldName)) {
                writtenFields.Add(fieldName);
                WriteTypeField(el);
            }
        }
        
        acType.WriteReaderMethod(this);
        acType.WriteReaderMethod(this, true);

        PopIndent();
        WriteLine("}"); // end class declr
        
        PopIndent();
        WriteLine("}"); // end namespace
        
        manager.EndBlock();
    }

    manager.Process(true);
#>