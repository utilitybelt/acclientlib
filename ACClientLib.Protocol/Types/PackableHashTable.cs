using System;
using System.ComponentModel;
using System.Collections.Generic;
using ACClientLib.Protocol.Lib;
using System.Drawing;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// HashTable which is packable for network
	/// </summary>
	[Description("HashTable which is packable for network")]
	public partial class PackableHashTable<T,U> : IACDataType {
		/// <summary>
		/// number of items in the table
		/// </summary>
		[Description("number of items in the table")]
		public ushort Count { get; set; }

		/// <summary>
		/// max size of the table
		/// </summary>
		[Description("max size of the table")]
		public ushort MaxSize { get; set; }

		/// <summary>
		/// The dictionary of items
		/// </summary>
		[Description("The dictionary of items")]
		public Dictionary<T, U> Table { get; set; }
        public ParsedFieldInfo? ParseInfo { get; set; }

        public PackableHashTable(IACProtocolReader reader, ParsedFieldInfo? parseInfo = null) {
			ParseInfo = parseInfo;
            var _pos = reader.Position;
            
			Count = reader.ReadUInt16();
            ParseInfo?.TrackField(ParseInfo, nameof(Count), Count, reader, reader.Position - _pos);
            _pos = reader.Position;
            
			MaxSize = reader.ReadUInt16();
            ParseInfo?.TrackField(ParseInfo, nameof(MaxSize), MaxSize, reader, reader.Position - _pos);
            _pos = reader.Position;

            Table = [];
			var tableParseInfo = ParseInfo is not null ? new ParsedFieldInfo(ParseInfo, nameof(Table), Table, reader, 0) : null;
            for (var i = 0; i < Count; i++) {
                var key = reader.ReadACDataType<T>(tableParseInfo);
                var val = reader.ReadACDataType<U>(tableParseInfo);
				Table.Add(key, val);
            }
			if (tableParseInfo is not null) {
				ParseInfo?.TrackField(ParseInfo, tableParseInfo, reader.Position - _pos);
			}
        }

		public PackableHashTable(Dictionary<T, U> table) {
			Count = (ushort)table.Count;
			MaxSize = (ushort)table.Count;
			Table = table;
		}

        public static implicit operator Dictionary<T, U>(PackableHashTable<T, U> source) => source.Table;
        public static implicit operator PackableHashTable<T, U>(Dictionary<T, U> source) => new(source);
    }
}
