﻿using ACClientLib.Protocol.Lib;

namespace ACClientLib.Protocol.Types {
	public partial struct DataId(uint value, ParsedFieldInfo? parseInfo = null) : IACDataType {
        public uint Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public override string ToString() {
            return $"0x{Value:X8}";
        }

        public static implicit operator uint(DataId source) => source.Value;
        public static implicit operator DataId(uint source) => new(source);
    }
}
