using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types {
    public partial struct SpellId(ushort value, ParsedFieldInfo? parseInfo = null) {
        public ushort Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public override string ToString() {
            return $"0x{Value:X4}";
        }

        public static implicit operator ushort(SpellId source) => source.Value;
        public static implicit operator SpellId(ushort source) => new(source);
    }
}
