using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using System.Xml.Linq;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types {
	public partial struct PackedDWORD(uint value, ParsedFieldInfo? parseInfo = null) {
        public uint Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public static implicit operator uint(PackedDWORD source) => source.Value;
        public static implicit operator PackedDWORD(uint source) => new(source);
    }
}
