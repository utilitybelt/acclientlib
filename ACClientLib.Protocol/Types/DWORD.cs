using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types {
	public partial struct DWORD(uint value, ParsedFieldInfo? parseInfo = null) : IACDataType {
        public uint Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public static implicit operator uint(DWORD source) => source.Value;
        public static implicit operator DWORD(uint source) => new(source);
    }
}
