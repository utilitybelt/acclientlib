﻿using ACClientLib.Protocol.Lib;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Types {
    public partial struct LandcellId(uint value, ParsedFieldInfo? parseInfo = null) : IACDataType {
        public uint Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public override string ToString() {
            return $"0x{Value:X8}";
        }

        public static implicit operator uint(LandcellId source) => source.Value;
        public static implicit operator LandcellId(uint source) => new(source);
    }
}
