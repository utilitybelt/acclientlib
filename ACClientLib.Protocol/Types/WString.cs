using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types
{
    public partial struct WString(string Value, ParsedFieldInfo? parseInfo = null) {
        public string Value { get; set; } = Value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public static implicit operator string(WString source) => source.Value;
        public static implicit operator WString(string source) => new(source);
    }
}
