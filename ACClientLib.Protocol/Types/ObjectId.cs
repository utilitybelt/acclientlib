using ACClientLib.Protocol.Lib;

namespace ACClientLib.Protocol.Types {
    /// <summary>
    /// A struct that represents a WorldObjectId. These are unique per object per server (they may be re-used after an object was deleted).
    /// </summary>
    /// <param name="value">The id</param>
	public partial struct ObjectId(uint value, ParsedFieldInfo? parseInfo = null) : IACDataType {
		public uint Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public override string ToString() {
            return $"0x{Value:X8}";
        }

        public static implicit operator uint(ObjectId source) => source.Value;
        public static implicit operator ObjectId(uint source) => new(source);
    }
}
