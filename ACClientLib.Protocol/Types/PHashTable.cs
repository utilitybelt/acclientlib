using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using System.Drawing;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types {
    /// <summary>
    /// HashTable which is packable for network
    /// </summary>
    [Description("HashTable which is packable for network")]
    public partial class PHashTable<T, U> : IACDataType {
        public uint PackedSize { get; set; }
        public ParsedFieldInfo? ParseInfo { get; set; }

        public Dictionary<T, U> Table { get; set; }

        public PHashTable(IACProtocolReader reader, ParsedFieldInfo? parseInfo = null) {
            ParseInfo = parseInfo;
            var _pos = reader.Position;

            PackedSize = reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(PackedSize), PackedSize, reader, reader.Position - _pos);
            _pos = reader.Position;

            Table = [];
            var tableParseInfo = ParseInfo is not null ? new ParsedFieldInfo(ParseInfo, nameof(Table), Table, reader, 0) : null;
            for (var i = 0; i < PackedSize; i++) {
                var key = reader.ReadACDataType<T>(tableParseInfo);
                var val = reader.ReadACDataType<U>(tableParseInfo);
                Table.Add(key, val);
            }
            if (tableParseInfo is not null) {
                ParseInfo?.TrackField(ParseInfo, tableParseInfo, reader.Position - _pos);
            }
        }

        public PHashTable(Dictionary<T, U> table) {
            Table = table;
        }

        public static implicit operator Dictionary<T, U>(PHashTable<T, U> source) => source.Table;
        public static implicit operator PHashTable<T, U>(Dictionary<T, U> source) => new(source);

    }
}
