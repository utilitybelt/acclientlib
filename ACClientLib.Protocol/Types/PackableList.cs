using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types {
    /// <summary>
    /// List which is packable for network
    /// </summary>
    [Description("List which is packable for network")]
    public partial class PackableList<T> : IACDataType {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        [Description("Number of items in the list")]
        public uint Count { get; set; }

        /// <summary>
        /// The list of items
        /// </summary>
        [Description("The list of items")]
        public List<T> Values { get; set; }
        public ParsedFieldInfo? ParseInfo { get; set; }

        public PackableList(IACProtocolReader reader, ParsedFieldInfo? parseInfo = null) {
            ParseInfo = parseInfo;
            var _pos = reader.Position;

            Count = reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(Count), Count, reader, reader.Position - _pos);
            _pos = reader.Position;

            Values = [];
            var tableParseInfo = ParseInfo is not null ? new ParsedFieldInfo(ParseInfo, nameof(Values), Values, reader, 0) : null;
            for (var i = 0; i < Count; i++) {
                var v = tableParseInfo is null ? null : new ParsedFieldInfo(tableParseInfo, typeof(T).Name, null, reader);
                var item = reader.ReadACDataType<T>(v);
                if (v is not null) {
                    tableParseInfo?.TrackField(tableParseInfo, v, v.Length);
                }
                if (item != null) {
                    Values.Add(item);
                }
            }
            if (tableParseInfo is not null) {
                ParseInfo?.TrackField(ParseInfo, tableParseInfo, reader.Position - _pos);
            }
        }

        public PackableList(List<T> values) {
            Values = values;
            Count = (uint)values.Count;
        }

        public static implicit operator List<T>(PackableList<T> source) => source.Values;
        public static implicit operator PackableList<T>(List<T> source) => new(source);
    }
}
