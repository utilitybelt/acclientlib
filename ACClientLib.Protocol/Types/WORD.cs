using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Lib;
namespace ACClientLib.Protocol.Types {
    public partial struct WORD(ushort value, ParsedFieldInfo? parseInfo = null) {
        public ushort Value { get; set; } = value;
        public ParsedFieldInfo? ParseInfo { get; set; } = parseInfo;

        public static implicit operator ushort(WORD source) => source.Value;
        public static implicit operator WORD(ushort source) => new(source);
    }
}
