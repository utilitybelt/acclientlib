﻿using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using static System.Net.Mime.MediaTypeNames;
using System.Xml.Linq;
using ACClientLib.Protocol.Types;

namespace ACClientLib.Protocol.Messages.C2S {
    /// <summary>
    /// Special message type that contains GameAction messages. These are ordered.
    /// </summary>
    [Description("Special message type that contains GameAction messages. These are ordered.")]
    public abstract partial class Ordered_GameAction : IC2SMessage {
        /// <inheritdoc />
        public C2SMessage OpCode => C2SMessage.Ordered_GameAction;

        /// <summary>
        /// Sequence id
        /// </summary>
        [Description("Sequence id")]
        public uint Sequence { get; set; }

        /// <summary>
        /// Type of GameAction being sent
        /// </summary>
        [Description("Type of GameAction being sent")]
        public GameAction ActionType { get; set; }
        public ParsedFieldInfo? ParseInfo { get; set; }

        public Ordered_GameAction(IACProtocolReader reader) {
            Sequence = reader.ReadUInt32();
            ActionType = (GameAction)reader.ReadUInt32();
        }

        public Ordered_GameAction(IACProtocolReader reader, ParsedFieldInfo? parseInfo) {
            ParseInfo = parseInfo;
            var _pos = reader.Position;

            Sequence = reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(Sequence), Sequence, reader, reader.Position - _pos);
            _pos = reader.Position;

            ActionType = (GameAction)reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(ActionType), ActionType, reader, reader.Position - _pos);
        }
    }
}
