﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using static System.Net.Mime.MediaTypeNames;
using System.Xml.Linq;
namespace ACClientLib.Protocol.Messages.S2C {
    /// <summary>
    /// Special message type that contains GameEvent messages. These are ordered.
    /// </summary>
    [Description("Special message type that contains GameEvent messages. These are ordered.")]
    public partial class Ordered_GameEvent : IS2CMessage {
        /// <inheritdoc />
        public S2CMessage OpCode => S2CMessage.Ordered_GameEvent;

        /// <summary>
        /// Should always be the current character id
        /// </summary>
        [Description("Should always be the current character id")]
        public ObjectId ObjectId { get; set; }

        /// <summary>
        /// Sequence id
        /// </summary>
        [Description("Sequence id")]
        public uint Sequence { get; set; }

        /// <summary>
        /// Type of GameEvent data to read
        /// </summary>
        [Description("Type of GameEvent data to read")]
        public GameEvent EventType { get; set; }
        public ParsedFieldInfo? ParseInfo { get; set ; }

        public Ordered_GameEvent(IACProtocolReader reader) {
            ObjectId = reader.ReadUInt32();
            Sequence = reader.ReadUInt32();
            EventType = (GameEvent)reader.ReadUInt32();
        }

        public Ordered_GameEvent(IACProtocolReader reader, ParsedFieldInfo? parseInfo) {
            ParseInfo = parseInfo;
            var _pos = reader.Position;

            ObjectId = reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(ObjectId), ObjectId, reader, reader.Position - _pos);
            _pos = reader.Position;

            Sequence = reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(Sequence), Sequence, reader, reader.Position - _pos);
            _pos = reader.Position;

            EventType = (GameEvent)reader.ReadUInt32();
            ParseInfo?.TrackField(ParseInfo, nameof(EventType), EventType, reader, reader.Position - _pos);
        }
    }
}
