﻿using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ACClientLib.Protocol {
    public class ProtoManager {
        public Dictionary<uint, ACFragment> PendingC2SFragments = new Dictionary<uint, ACFragment>();
        public Dictionary<uint, ACFragment> PendingS2CFragments = new Dictionary<uint, ACFragment>();

        public C2SMessageReader C2S { get; } = new();
        public S2CMessageReader S2C { get; } = new();

        public ProtoManager() {
        }

        public List<C2SPacket> HandleC2SData(IACProtocolReader reader, int length, ParsedFieldInfo? parseInfo = null) {
            var packets = new List<C2SPacket>();
            var start = reader.Position;
            while (reader.Position < start + length) {
                var startOffset = reader.Position;
                var childParseInfo = parseInfo is null ? null : new ParsedFieldInfo(parseInfo, $"C2SPacket", null, reader);
                var packet = childParseInfo is null ? new C2SPacket(reader) : new C2SPacket(reader, childParseInfo);
                var iPacket = (IACPacket)packet;

                if (packet.ParseInfo is not null) {
                    packet.ParseInfo.Value = packet;
                }

                if (iPacket.Fragments is not null) {
                    ACFragment frag;
                    if (!PendingC2SFragments.TryGetValue(iPacket.Fragments.Sequence, out frag)) {
                        frag = new ACFragment(iPacket.Fragments.Sequence, iPacket.Fragments.Count);
                        PendingC2SFragments.Add(iPacket.Fragments.Sequence, frag);
                    }
                    if (frag.Received == frag.Count) {
                        try {
                            PendingC2SFragments.Remove(iPacket.Fragments.Sequence);
                            var fragReader = new ACBinaryReader(frag.Data);
                            var fragParseInfo = parseInfo is null ? null : new ParsedFieldInfo(parseInfo, nameof(packet.Messages), null, fragReader);
                            //while (fragReader.Position < fragReader.Length) {
                            var msg = C2S.HandleMessageData(fragReader, frag.Data.Length, fragParseInfo);
                            if (msg is not null) {
                                packet.Messages ??= [];
                                packet.Messages.Add(msg);
                            }
                            else {
                                Console.WriteLine($"Message was null?: {string.Join(" ", frag.Data.ToArray().Select(b => $"{b:X2}"))}");
                            }
                            if (fragReader.Position < frag.Length) {
                                Console.WriteLine($"Frag reader ended early... {fragReader.Position} / {frag.Length} (next is: {string.Join(" ", fragReader.Data.Slice(fragReader.Position, frag.Length).ToArray().Select(b => $"{b:X2}"))})");
                            }
                        }
                        catch (Exception e) {
                            Console.WriteLine(e.ToString());
                            Console.WriteLine(iPacket?.ParseInfo?.ToString(1).ToString());
                        }
                        //}
                    }
                }

                packets.Add(packet);
            }

            return packets;
        }

        public List<S2CPacket> HandleS2CData(IACProtocolReader reader, int length, ParsedFieldInfo? parseInfo = null) {
            var packets = new List<S2CPacket>();
            var start = reader.Position;
            while (reader.Position < start + length) {
                var startOffset = reader.Position;
                var childParseInfo = parseInfo is null ? null : new ParsedFieldInfo(parseInfo, $"S2CPacket", null, reader);
                var packet = childParseInfo is null ? new S2CPacket(reader) : new S2CPacket(reader, childParseInfo);
                var iPacket = (IACPacket)packet;

                if (packet.ParseInfo is not null) {
                    packet.ParseInfo.Value = packet;
                }

                if (iPacket.Fragments is not null) {
                    ACFragment frag;
                    if (!PendingS2CFragments.TryGetValue(iPacket.Fragments.Sequence, out frag)) {
                        frag = new ACFragment(iPacket.Fragments.Sequence, iPacket.Fragments.Count);
                        PendingS2CFragments.Add(iPacket.Fragments.Sequence, frag);
                    }

                    frag.AddChunk(iPacket.Fragments);

                    if (frag.Received == frag.Count) {
                        try {
                            PendingS2CFragments.Remove(iPacket.Fragments.Sequence);
                            var fragReader = new ACBinaryReader(frag.Data);
                            var fragParseInfo = parseInfo is null ? null : new ParsedFieldInfo(parseInfo, nameof(packet.Messages), null, fragReader);
                            //while (fragReader.Position < fragReader.Length - 4) {
                            var msg = S2C.HandleMessageData(fragReader, frag.Data.Length, fragParseInfo);
                            if (msg is not null) {
                                packet.Messages ??= [];
                                packet.Messages.Add(msg);
                            }
                            else {
                                Console.WriteLine($"Message was null?: {string.Join(" ", frag.Data.ToArray().Select(b => $"{b:X2}"))}");
                            }
                            if (fragReader.Position < frag.Length) {
                                Console.WriteLine($"Frag reader ended early... {fragReader.Position} / {frag.Length} (next is: {string.Join(" ", fragReader.Data.Slice(fragReader.Position, frag.Length).ToArray().Select(b => $"{b:X2}"))})");
                            }
                        }
                        catch (Exception e) {
                            Console.WriteLine(e.ToString());
                            Console.WriteLine(iPacket?.ParseInfo?.ToString(1).ToString());
                        }
                        //}
                    }
                }

                packets.Add(packet);
            }

            return packets;
        }
    }
}
