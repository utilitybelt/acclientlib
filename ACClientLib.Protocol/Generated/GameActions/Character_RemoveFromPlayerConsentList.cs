// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Remove your corpse looting permission for the given player, /consent remove 
	/// </summary>
	[Description("Remove your corpse looting permission for the given player, /consent remove ")]
	public partial class Character_RemoveFromPlayerConsentList : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Character_RemoveFromPlayerConsentList;

		/// <summary>
		/// Name of player to remove permission to loot the playes corpses
		/// </summary>
		[Description("Name of player to remove permission to loot the playes corpses")]
		public string TargetName { get; set; }

		public Character_RemoveFromPlayerConsentList(IACProtocolReader reader) : base(reader) {
			TargetName = reader.ReadString();

		}
		public Character_RemoveFromPlayerConsentList(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			TargetName = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(TargetName), TargetName, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
