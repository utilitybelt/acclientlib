// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Performs a movement based on input
	/// </summary>
	[Description("Performs a movement based on input")]
	public partial class Movement_DoMovementCommand : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Movement_DoMovementCommand;

		/// <summary>
		/// motion command
		/// </summary>
		[Description("motion command")]
		public uint Motion { get; set; }

		/// <summary>
		/// speed of movement
		/// </summary>
		[Description("speed of movement")]
		public float Speed { get; set; }

		/// <summary>
		/// run key being held
		/// </summary>
		[Description("run key being held")]
		public HoldKey HoldKey { get; set; }

		public Movement_DoMovementCommand(IACProtocolReader reader) : base(reader) {
			Motion = reader.ReadUInt32();

			Speed = reader.ReadSingle();

			HoldKey = (HoldKey)reader.ReadUInt32();

		}
		public Movement_DoMovementCommand(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			Motion = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Motion), Motion, reader, reader.Position - _pos);
			_pos = reader.Position;

			Speed = reader.ReadSingle();
			ParseInfo?.TrackField(ParseInfo, nameof(Speed), Speed, reader, reader.Position - _pos);
			_pos = reader.Position;

			HoldKey = (HoldKey)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(HoldKey), HoldKey, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
