// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Abandons a contract
	/// </summary>
	[Description("Abandons a contract")]
	public partial class Social_AbandonContract : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Social_AbandonContract;

		/// <summary>
		/// Id of contact being abandoned
		/// </summary>
		[Description("Id of contact being abandoned")]
		public ContractId ContractId { get; set; }

		public Social_AbandonContract(IACProtocolReader reader) : base(reader) {
			ContractId = (ContractId)reader.ReadUInt32();

		}
		public Social_AbandonContract(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			ContractId = (ContractId)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ContractId), ContractId, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
