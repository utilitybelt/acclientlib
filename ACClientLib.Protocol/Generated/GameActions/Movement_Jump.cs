// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Performs a jump
	/// </summary>
	[Description("Performs a jump")]
	public partial class Movement_Jump : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Movement_Jump;

		/// <summary>
		/// set of jumping data
		/// </summary>
		[Description("set of jumping data")]
		public JumpPack Jump { get; set; }

		public Movement_Jump(IACProtocolReader reader) : base(reader) {
			Jump = new(reader);

		}
		public Movement_Jump(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			Jump = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Jump), Jump, reader));
			ParseInfo?.TrackField(ParseInfo, Jump.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
