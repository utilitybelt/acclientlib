// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Spend skill credits to train a skill.
	/// </summary>
	[Description("Spend skill credits to train a skill.")]
	public partial class Train_TrainSkillAdvancementClass : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Train_TrainSkillAdvancementClass;

		/// <summary>
		/// The Id of the skill
		/// </summary>
		[Description("The Id of the skill")]
		public SkillId Skill { get; set; }

		/// <summary>
		/// The number of skill credits being spent
		/// </summary>
		[Description("The number of skill credits being spent")]
		public uint Credits { get; set; }

		public Train_TrainSkillAdvancementClass(IACProtocolReader reader) : base(reader) {
			Skill = (SkillId)reader.ReadInt32();

			Credits = reader.ReadUInt32();

		}
		public Train_TrainSkillAdvancementClass(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			Skill = (SkillId)reader.ReadInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Skill), Skill, reader, reader.Position - _pos);
			_pos = reader.Position;

			Credits = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Credits), Credits, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
