// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Add an item to the shortcut bar.
	/// </summary>
	[Description("Add an item to the shortcut bar.")]
	public partial class Character_AddShortCut : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Character_AddShortCut;

		/// <summary>
		/// Shortcut information
		/// </summary>
		[Description("Shortcut information")]
		public ShortCutData Shortcut { get; set; }

		public Character_AddShortCut(IACProtocolReader reader) : base(reader) {
			Shortcut = new(reader);

		}
		public Character_AddShortCut(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			Shortcut = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Shortcut), Shortcut, reader));
			ParseInfo?.TrackField(ParseInfo, Shortcut.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
