// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Removes a specific player from your house guest list, /house guest remove
	/// </summary>
	[Description("Removes a specific player from your house guest list, /house guest remove")]
	public partial class House_RemovePermanentGuest : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.House_RemovePermanentGuest;

		/// <summary>
		/// Player name to remove from your house guest list
		/// </summary>
		[Description("Player name to remove from your house guest list")]
		public string GuestName { get; set; }

		public House_RemovePermanentGuest(IACProtocolReader reader) : base(reader) {
			GuestName = reader.ReadString();

		}
		public House_RemovePermanentGuest(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			GuestName = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(GuestName), GuestName, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
