// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Store an item in a container.
	/// </summary>
	[Description("Store an item in a container.")]
	public partial class Inventory_PutItemInContainer : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Inventory_PutItemInContainer;

		/// <summary>
		/// The item being stored
		/// </summary>
		[Description("The item being stored")]
		public ObjectId ObjectId { get; set; }

		/// <summary>
		/// The container the item is being stored in
		/// </summary>
		[Description("The container the item is being stored in")]
		public ObjectId ContainerId { get; set; }

		/// <summary>
		/// The position in the container where the item is being placed
		/// </summary>
		[Description("The position in the container where the item is being placed")]
		public uint SlotIndex { get; set; }

		public Inventory_PutItemInContainer(IACProtocolReader reader) : base(reader) {
			ObjectId = reader.ReadUInt32();

			ContainerId = reader.ReadUInt32();

			SlotIndex = reader.ReadUInt32();

		}
		public Inventory_PutItemInContainer(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			ObjectId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ObjectId), ObjectId, reader, reader.Position - _pos);
			_pos = reader.Position;

			ContainerId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ContainerId), ContainerId, reader, reader.Position - _pos);
			_pos = reader.Position;

			SlotIndex = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(SlotIndex), SlotIndex, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
