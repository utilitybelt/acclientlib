// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Offer or confirm stalemate
	/// </summary>
	[Description("Offer or confirm stalemate")]
	public partial class Game_Stalemate : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Game_Stalemate;

		/// <summary>
		/// Whether stalemate offer is active or not
		/// </summary>
		[Description("Whether stalemate offer is active or not")]
		public bool On { get; set; }

		public Game_Stalemate(IACProtocolReader reader) : base(reader) {
			On = reader.ReadBool();

		}
		public Game_Stalemate(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			On = reader.ReadBool();
			ParseInfo?.TrackField(ParseInfo, nameof(On), On, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
