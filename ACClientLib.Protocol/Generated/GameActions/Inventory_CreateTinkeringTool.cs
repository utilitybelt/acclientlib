// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameActionTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.C2S;
namespace ACClientLib.Protocol.GameActions {
	/// <summary>
	/// Salvages items
	/// </summary>
	[Description("Salvages items")]
	public partial class Inventory_CreateTinkeringTool : Ordered_GameAction, IGameAction {
		/// <inheritdoc />
		public GameAction ActionOpCode => GameAction.Inventory_CreateTinkeringTool;

		public ObjectId ToolId { get; set; }

		/// <summary>
		/// Set of object Id's to be salvaged
		/// </summary>
		[Description("Set of object Id's to be salvaged")]
		public PackableList<ObjectId> Items { get; set; }

		public Inventory_CreateTinkeringTool(IACProtocolReader reader) : base(reader) {
			ToolId = reader.ReadUInt32();

			Items = new(reader);

		}
		public Inventory_CreateTinkeringTool(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			ToolId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ToolId), ToolId, reader, reader.Position - _pos);
			_pos = reader.Position;

			Items = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Items), Items, reader));
			ParseInfo?.TrackField(ParseInfo, Items.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
