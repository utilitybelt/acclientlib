// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Full spell Id combining the spell id with the spell layer.
	/// </summary>
	[Description("Full spell Id combining the spell id with the spell layer.")]
	public partial class LayeredSpellId : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// Id of the spell
		/// </summary>
		[Description("Id of the spell")]
		public SpellId Id { get; set; }

		/// <summary>
		/// Layer of the spell, seperating multiple instances of the same spell
		/// </summary>
		[Description("Layer of the spell, seperating multiple instances of the same spell")]
		public ushort Layer { get; set; }

		public LayeredSpellId(IACProtocolReader reader) {
			Id = reader.ReadUInt16();

			Layer = reader.ReadUInt16();

		}
		public LayeredSpellId(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Id = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(Id), Id, reader, reader.Position - _pos);
			_pos = reader.Position;

			Layer = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(Layer), Layer, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
