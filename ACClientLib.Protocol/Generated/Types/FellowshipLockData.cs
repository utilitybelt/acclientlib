// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	public partial class FellowshipLockData : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// Always 0 in captures so far
		/// </summary>
		[Description("Always 0 in captures so far")]
		public uint Unknown1 { get; set; }

		/// <summary>
		/// Value varies. May be 2 WORDs
		/// </summary>
		[Description("Value varies. May be 2 WORDs")]
		public uint Unknown2 { get; set; }

		/// <summary>
		/// Appears to be some kind of timestamp
		/// </summary>
		[Description("Appears to be some kind of timestamp")]
		public uint Unknown3 { get; set; }

		public uint Timestamp { get; set; }

		public uint Sequence { get; set; }

		public FellowshipLockData(IACProtocolReader reader) {
			Unknown1 = reader.ReadUInt32();

			Unknown2 = reader.ReadUInt32();

			Unknown3 = reader.ReadUInt32();

			Timestamp = reader.ReadUInt32();

			Sequence = reader.ReadUInt32();

		}
		public FellowshipLockData(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Unknown1 = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Unknown1), Unknown1, reader, reader.Position - _pos);
			_pos = reader.Position;

			Unknown2 = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Unknown2), Unknown2, reader, reader.Position - _pos);
			_pos = reader.Position;

			Unknown3 = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Unknown3), Unknown3, reader, reader.Position - _pos);
			_pos = reader.Position;

			Timestamp = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Timestamp), Timestamp, reader, reader.Position - _pos);
			_pos = reader.Position;

			Sequence = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Sequence), Sequence, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
