// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// The Skill structure contains information about a character skill.
	/// </summary>
	[Description("The Skill structure contains information about a character skill.")]
	public partial class Skill : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// points raised
		/// </summary>
		[Description("points raised")]
		public ushort PointsRaised { get; set; }

		/// <summary>
		/// If this is not 0, it appears to trigger the initLevel to be treated as extra XP applied to the skill
		/// </summary>
		[Description("If this is not 0, it appears to trigger the initLevel to be treated as extra XP applied to the skill")]
		public ushort AdjustPP { get; set; }

		/// <summary>
		/// skill state
		/// </summary>
		[Description("skill state")]
		public SkillAdvancementClass TrainingLevel { get; set; }

		/// <summary>
		/// XP spent on this skill
		/// </summary>
		[Description("XP spent on this skill")]
		public uint ExperienceSpent { get; set; }

		/// <summary>
		/// starting point for advancement of the skill (eg bonus points)
		/// </summary>
		[Description("starting point for advancement of the skill (eg bonus points)")]
		public uint InnatePoints { get; set; }

		/// <summary>
		/// last use difficulty
		/// </summary>
		[Description("last use difficulty")]
		public uint ResistanceOfLastCheck { get; set; }

		/// <summary>
		/// time skill was last used
		/// </summary>
		[Description("time skill was last used")]
		public double LastUsedTime { get; set; }

		public Skill(IACProtocolReader reader) {
			PointsRaised = reader.ReadUInt16();

			AdjustPP = reader.ReadUInt16();

			TrainingLevel = (SkillAdvancementClass)reader.ReadUInt32();

			ExperienceSpent = reader.ReadUInt32();

			InnatePoints = reader.ReadUInt32();

			ResistanceOfLastCheck = reader.ReadUInt32();

			LastUsedTime = reader.ReadDouble();

		}
		public Skill(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			PointsRaised = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(PointsRaised), PointsRaised, reader, reader.Position - _pos);
			_pos = reader.Position;

			AdjustPP = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(AdjustPP), AdjustPP, reader, reader.Position - _pos);
			_pos = reader.Position;

			TrainingLevel = (SkillAdvancementClass)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(TrainingLevel), TrainingLevel, reader, reader.Position - _pos);
			_pos = reader.Position;

			ExperienceSpent = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ExperienceSpent), ExperienceSpent, reader, reader.Position - _pos);
			_pos = reader.Position;

			InnatePoints = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(InnatePoints), InnatePoints, reader, reader.Position - _pos);
			_pos = reader.Position;

			ResistanceOfLastCheck = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ResistanceOfLastCheck), ResistanceOfLastCheck, reader, reader.Position - _pos);
			_pos = reader.Position;

			LastUsedTime = reader.ReadDouble();
			ParseInfo?.TrackField(ParseInfo, nameof(LastUsedTime), LastUsedTime, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
