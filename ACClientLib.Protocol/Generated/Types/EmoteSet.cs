// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	public partial class EmoteSet : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		public EmoteCategory Category { get; set; }

		public float Probability { get; set; }

		public uint ClassId { get; set; }

		public uint Style { get; set; }

		public uint Substyle { get; set; }

		public string? Quest { get; set; }

		public uint VendorType { get; set; }

		public float MinHealth { get; set; }

		public float MaxHealth { get; set; }

		/// <summary>
		/// List of emotes
		/// </summary>
		[Description("List of emotes")]
		public PackableList<Emote> Emotes { get; set; }

		public EmoteSet(IACProtocolReader reader) {
			Category = (EmoteCategory)reader.ReadUInt32();

			Probability = reader.ReadSingle();

			switch ((int)Category) {
				case 0x01:
				case 0x06:
					ClassId = reader.ReadUInt32();
					break;
				case 0x05:
					Style = reader.ReadUInt32();
					Substyle = reader.ReadUInt32();
					break;
				case 0x0C:
				case 0x0D:
				case 0x16:
				case 0x17:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x25:
				case 0x26:
					Quest = reader.ReadString();
					break;
				case 0x02:
					VendorType = reader.ReadUInt32();
					break;
				case 0x0F:
					MinHealth = reader.ReadSingle();
					MaxHealth = reader.ReadSingle();
					break;
			}

			Emotes = new(reader);

		}
		public EmoteSet(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Category = (EmoteCategory)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Category), Category, reader, reader.Position - _pos);
			_pos = reader.Position;

			Probability = reader.ReadSingle();
			ParseInfo?.TrackField(ParseInfo, nameof(Probability), Probability, reader, reader.Position - _pos);
			_pos = reader.Position;

			switch ((int)Category) {
				case 0x01:
				case 0x06:
					ClassId = reader.ReadUInt32();
					ParseInfo?.TrackField(ParseInfo, nameof(ClassId), ClassId, reader, reader.Position - _pos);
					_pos = reader.Position;
					break;
				case 0x05:
					Style = reader.ReadUInt32();
					ParseInfo?.TrackField(ParseInfo, nameof(Style), Style, reader, reader.Position - _pos);
					_pos = reader.Position;
					Substyle = reader.ReadUInt32();
					ParseInfo?.TrackField(ParseInfo, nameof(Substyle), Substyle, reader, reader.Position - _pos);
					_pos = reader.Position;
					break;
				case 0x0C:
				case 0x0D:
				case 0x16:
				case 0x17:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x25:
				case 0x26:
					Quest = reader.ReadString();
					ParseInfo?.TrackField(ParseInfo, nameof(Quest), Quest, reader, reader.Position - _pos);
					_pos = reader.Position;
					break;
				case 0x02:
					VendorType = reader.ReadUInt32();
					ParseInfo?.TrackField(ParseInfo, nameof(VendorType), VendorType, reader, reader.Position - _pos);
					_pos = reader.Position;
					break;
				case 0x0F:
					MinHealth = reader.ReadSingle();
					ParseInfo?.TrackField(ParseInfo, nameof(MinHealth), MinHealth, reader, reader.Position - _pos);
					_pos = reader.Position;
					MaxHealth = reader.ReadSingle();
					ParseInfo?.TrackField(ParseInfo, nameof(MaxHealth), MaxHealth, reader, reader.Position - _pos);
					_pos = reader.Position;
					break;
			}

			Emotes = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Emotes), Emotes, reader));
			ParseInfo?.TrackField(ParseInfo, Emotes.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
