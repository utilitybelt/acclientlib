// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Contains data for animation part changes
	/// </summary>
	[Description("Contains data for animation part changes")]
	public partial class AnimPartChange : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// The index of the model
		/// </summary>
		[Description("The index of the model")]
		public byte PartIndex { get; set; }

		/// <summary>
		/// model DataId (minus 0x01000000)
		/// </summary>
		[Description("model DataId (minus 0x01000000)")]
		public DataId PartId { get; set; }

		public AnimPartChange(IACProtocolReader reader) {
			PartIndex = reader.ReadByte();

			PartId = reader.ReadUInt32();

		}
		public AnimPartChange(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			PartIndex = reader.ReadByte();
			ParseInfo?.TrackField(ParseInfo, nameof(PartIndex), PartIndex, reader, reader.Position - _pos);
			_pos = reader.Position;

			PartId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(PartId), PartId, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
