// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Set of information related to purchasing a housing
	/// </summary>
	[Description("Set of information related to purchasing a housing")]
	public partial class HouseProfile : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// the number associated with this dwelling
		/// </summary>
		[Description("the number associated with this dwelling")]
		public uint DwellingId { get; set; }

		/// <summary>
		/// the object Id of the the current owner
		/// </summary>
		[Description("the object Id of the the current owner")]
		public ObjectId OwnerId { get; set; }

		public HouseBitfield Flags { get; set; }

		/// <summary>
		/// the level requirement to purchase this dwelling (-1 if no requirement)
		/// </summary>
		[Description("the level requirement to purchase this dwelling (-1 if no requirement)")]
		public int MinLevel { get; set; }

		public int MaxLevel { get; set; }

		/// <summary>
		/// the rank requirement to purchase this dwelling (-1 if no requirement)
		/// </summary>
		[Description("the rank requirement to purchase this dwelling (-1 if no requirement)")]
		public int MinAllegRank { get; set; }

		public int MaxAllegRank { get; set; }

		public bool MaintenanceFree { get; set; }

		/// <summary>
		/// the type of dwelling (1=cottage, 2=villa, 3=mansion, 4=apartment)
		/// </summary>
		[Description("the type of dwelling (1=cottage, 2=villa, 3=mansion, 4=apartment)")]
		public HouseType Type { get; set; }

		/// <summary>
		/// the name of the current owner
		/// </summary>
		[Description("the name of the current owner")]
		public string OwnerName { get; set; }

		public PackableList<HousePayment> Buy { get; set; }

		public PackableList<HousePayment> Rent { get; set; }

		public HouseProfile(IACProtocolReader reader) {
			DwellingId = reader.ReadUInt32();

			OwnerId = reader.ReadUInt32();

			Flags = (HouseBitfield)reader.ReadUInt32();

			MinLevel = reader.ReadInt32();

			MaxLevel = reader.ReadInt32();

			MinAllegRank = reader.ReadInt32();

			MaxAllegRank = reader.ReadInt32();

			MaintenanceFree = reader.ReadBool();

			Type = (HouseType)reader.ReadUInt32();

			OwnerName = reader.ReadString();

			Buy = new(reader);

			Rent = new(reader);

		}
		public HouseProfile(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			DwellingId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(DwellingId), DwellingId, reader, reader.Position - _pos);
			_pos = reader.Position;

			OwnerId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(OwnerId), OwnerId, reader, reader.Position - _pos);
			_pos = reader.Position;

			Flags = (HouseBitfield)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Flags), Flags, reader, reader.Position - _pos);
			_pos = reader.Position;

			MinLevel = reader.ReadInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(MinLevel), MinLevel, reader, reader.Position - _pos);
			_pos = reader.Position;

			MaxLevel = reader.ReadInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(MaxLevel), MaxLevel, reader, reader.Position - _pos);
			_pos = reader.Position;

			MinAllegRank = reader.ReadInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(MinAllegRank), MinAllegRank, reader, reader.Position - _pos);
			_pos = reader.Position;

			MaxAllegRank = reader.ReadInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(MaxAllegRank), MaxAllegRank, reader, reader.Position - _pos);
			_pos = reader.Position;

			MaintenanceFree = reader.ReadBool();
			ParseInfo?.TrackField(ParseInfo, nameof(MaintenanceFree), MaintenanceFree, reader, reader.Position - _pos);
			_pos = reader.Position;

			Type = (HouseType)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Type), Type, reader, reader.Position - _pos);
			_pos = reader.Position;

			OwnerName = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(OwnerName), OwnerName, reader, reader.Position - _pos);
			_pos = reader.Position;

			Buy = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Buy), Buy, reader));
			ParseInfo?.TrackField(ParseInfo, Buy.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

			Rent = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Rent), Rent, reader));
			ParseInfo?.TrackField(ParseInfo, Rent.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
