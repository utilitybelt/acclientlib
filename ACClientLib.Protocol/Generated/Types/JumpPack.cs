// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// A jump with sequences
	/// </summary>
	[Description("A jump with sequences")]
	public partial class JumpPack : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// Power of jump?
		/// </summary>
		[Description("Power of jump?")]
		public float Extent { get; set; }

		/// <summary>
		/// Velocity data
		/// </summary>
		[Description("Velocity data")]
		public Vector3 Velocity { get; set; }

		/// <summary>
		/// The instance sequence value for the object (number of logins for players)
		/// </summary>
		[Description("The instance sequence value for the object (number of logins for players)")]
		public ushort ObjectInstanceSequence { get; set; }

		/// <summary>
		/// The server control sequence value for the object
		/// </summary>
		[Description("The server control sequence value for the object")]
		public ushort ObjectServerControlSequence { get; set; }

		/// <summary>
		/// The teleport sequence value for the object
		/// </summary>
		[Description("The teleport sequence value for the object")]
		public ushort ObjectTeleportSequence { get; set; }

		/// <summary>
		/// The forced position sequence value for the object
		/// </summary>
		[Description("The forced position sequence value for the object")]
		public ushort ObjectForcePositionSequence { get; set; }

		public JumpPack(IACProtocolReader reader) {
			Extent = reader.ReadSingle();

			Velocity = reader.ReadVector3();

			ObjectInstanceSequence = reader.ReadUInt16();

			ObjectServerControlSequence = reader.ReadUInt16();

			ObjectTeleportSequence = reader.ReadUInt16();

			ObjectForcePositionSequence = reader.ReadUInt16();

			if (reader.Position % 4 > 0) reader.Position += 4 - (reader.Position % 4);

		}
		public JumpPack(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Extent = reader.ReadSingle();
			ParseInfo?.TrackField(ParseInfo, nameof(Extent), Extent, reader, reader.Position - _pos);
			_pos = reader.Position;

			Velocity = reader.ReadVector3();
			ParseInfo?.TrackField(ParseInfo, nameof(Velocity), Velocity, reader, reader.Position - _pos);
			_pos = reader.Position;

			ObjectInstanceSequence = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(ObjectInstanceSequence), ObjectInstanceSequence, reader, reader.Position - _pos);
			_pos = reader.Position;

			ObjectServerControlSequence = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(ObjectServerControlSequence), ObjectServerControlSequence, reader, reader.Position - _pos);
			_pos = reader.Position;

			ObjectTeleportSequence = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(ObjectTeleportSequence), ObjectTeleportSequence, reader, reader.Position - _pos);
			_pos = reader.Position;

			ObjectForcePositionSequence = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(ObjectForcePositionSequence), ObjectForcePositionSequence, reader, reader.Position - _pos);
			_pos = reader.Position;

			if (reader.Position % 4 > 0) reader.Position += 4 - (reader.Position % 4);

		}
	}
}
