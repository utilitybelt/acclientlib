// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Set of inventory items
	/// </summary>
	[Description("Set of inventory items")]
	public partial class GeneratorQueue : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		public PackableList<GeneratorQueueNode> Queue { get; set; }

		public GeneratorQueue(IACProtocolReader reader) {
			Queue = new(reader);

		}
		public GeneratorQueue(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Queue = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Queue), Queue, reader));
			ParseInfo?.TrackField(ParseInfo, Queue.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
