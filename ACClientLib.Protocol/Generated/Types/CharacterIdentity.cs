// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Basic information for a character used at the Login screen
	/// </summary>
	[Description("Basic information for a character used at the Login screen")]
	public partial class CharacterIdentity : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// The character Id for this entry.
		/// </summary>
		[Description("The character Id for this entry.")]
		public ObjectId CharacterId { get; set; }

		/// <summary>
		/// The name of this character.
		/// </summary>
		[Description("The name of this character.")]
		public string Name { get; set; }

		/// <summary>
		/// When 0, this character is not being deleted (not shown crossed out). Otherwise, it's a countdown timer in the number of seconds until the character is submitted for deletion.
		/// </summary>
		[Description("When 0, this character is not being deleted (not shown crossed out). Otherwise, it's a countdown timer in the number of seconds until the character is submitted for deletion.")]
		public uint SecondsGreyedOut { get; set; }

		public CharacterIdentity(IACProtocolReader reader) {
			CharacterId = reader.ReadUInt32();

			Name = reader.ReadString();

			SecondsGreyedOut = reader.ReadUInt32();

			if (reader.Position % 4 > 0) reader.Position += 4 - (reader.Position % 4);

		}
		public CharacterIdentity(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			CharacterId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(CharacterId), CharacterId, reader, reader.Position - _pos);
			_pos = reader.Position;

			Name = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(Name), Name, reader, reader.Position - _pos);
			_pos = reader.Position;

			SecondsGreyedOut = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(SecondsGreyedOut), SecondsGreyedOut, reader, reader.Position - _pos);
			_pos = reader.Position;

			if (reader.Position % 4 > 0) reader.Position += 4 - (reader.Position % 4);

		}
	}
}
