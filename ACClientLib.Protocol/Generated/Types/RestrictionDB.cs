// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// The RestrictionDB contains the access control list for a dwelling object.
	/// </summary>
	[Description("The RestrictionDB contains the access control list for a dwelling object.")]
	public partial class RestrictionDB : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// If high word is not 0, this value indicates the version of the message.
		/// </summary>
		[Description("If high word is not 0, this value indicates the version of the message.")]
		public uint Version { get; set; }

		/// <summary>
		/// 0 = private dwelling, 1 = open to public
		/// </summary>
		[Description("0 = private dwelling, 1 = open to public")]
		public uint Flags { get; set; }

		/// <summary>
		/// allegiance monarch (if allegiance access granted)
		/// </summary>
		[Description("allegiance monarch (if allegiance access granted)")]
		public ObjectId MonarchId { get; set; }

		/// <summary>
		/// Set of permissions on a per user basis. Key is the character id, value is 0 = dwelling access only, 1 = storage access also
		/// </summary>
		[Description("Set of permissions on a per user basis. Key is the character id, value is 0 = dwelling access only, 1 = storage access also")]
		public PHashTable<ObjectId, uint> Permissions { get; set; }

		public RestrictionDB(IACProtocolReader reader) {
			Version = reader.ReadUInt32();

			Flags = reader.ReadUInt32();

			MonarchId = reader.ReadUInt32();

			Permissions = new(reader);

		}
		public RestrictionDB(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Version = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Version), Version, reader, reader.Position - _pos);
			_pos = reader.Position;

			Flags = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Flags), Flags, reader, reader.Position - _pos);
			_pos = reader.Position;

			MonarchId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(MonarchId), MonarchId, reader, reader.Position - _pos);
			_pos = reader.Position;

			Permissions = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Permissions), Permissions, reader));
			ParseInfo?.TrackField(ParseInfo, Permissions.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
