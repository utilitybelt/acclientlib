// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Set of information related to a salvage of an item
	/// </summary>
	[Description("Set of information related to a salvage of an item")]
	public partial class SalvageResult : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		public MaterialType Material { get; set; }

		public double Workmanship { get; set; }

		public uint Units { get; set; }

		public SalvageResult(IACProtocolReader reader) {
			Material = (MaterialType)reader.ReadUInt32();

			Workmanship = reader.ReadDouble();

			Units = reader.ReadUInt32();

		}
		public SalvageResult(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Material = (MaterialType)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Material), Material, reader, reader.Position - _pos);
			_pos = reader.Position;

			Workmanship = reader.ReadDouble();
			ParseInfo?.TrackField(ParseInfo, nameof(Workmanship), Workmanship, reader, reader.Position - _pos);
			_pos = reader.Position;

			Units = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Units), Units, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
