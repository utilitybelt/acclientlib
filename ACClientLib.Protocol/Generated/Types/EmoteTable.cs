// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Contains a list of emotes for NPCs? Unknown what this does currently.
	/// </summary>
	[Description("Contains a list of emotes for NPCs? Unknown what this does currently.")]
	public partial class EmoteTable : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <summary>
		/// Key may be an EmoteCategory?
		/// </summary>
		[Description("Key may be an EmoteCategory?")]
		public PackableHashTable<EmoteCategory, EmoteSetList> Emotes { get; set; }

		public EmoteTable(IACProtocolReader reader) {
			Emotes = new(reader);

		}
		public EmoteTable(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			Emotes = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Emotes), Emotes, reader));
			ParseInfo?.TrackField(ParseInfo, Emotes.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
