// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Types {
	/// <summary>
	/// Optional header data when PacketHeaderFlags includes ConnectRequest
	/// </summary>
	[Description("Optional header data when PacketHeaderFlags includes ConnectRequest")]
	public partial class ConnectRequestHeader : IACDataType {
		public ParsedFieldInfo? ParseInfo { get; set; }

		public double ServerTime { get; set; }

		public ulong Cookie { get; set; }

		public int NetID { get; set; }

		public uint OutgoingSeed { get; set; }

		public uint IncomingSeed { get; set; }

		public DWORD Unknown { get; set; }

		public ConnectRequestHeader(IACProtocolReader reader) {
			ServerTime = reader.ReadDouble();

			Cookie = reader.ReadUInt64();

			NetID = reader.ReadInt32();

			OutgoingSeed = reader.ReadUInt32();

			IncomingSeed = reader.ReadUInt32();

			Unknown = reader.ReadUInt32();

		}
		public ConnectRequestHeader(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			ServerTime = reader.ReadDouble();
			ParseInfo?.TrackField(ParseInfo, nameof(ServerTime), ServerTime, reader, reader.Position - _pos);
			_pos = reader.Position;

			Cookie = reader.ReadUInt64();
			ParseInfo?.TrackField(ParseInfo, nameof(Cookie), Cookie, reader, reader.Position - _pos);
			_pos = reader.Position;

			NetID = reader.ReadInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(NetID), NetID, reader, reader.Position - _pos);
			_pos = reader.Position;

			OutgoingSeed = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(OutgoingSeed), OutgoingSeed, reader, reader.Position - _pos);
			_pos = reader.Position;

			IncomingSeed = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(IncomingSeed), IncomingSeed, reader, reader.Position - _pos);
			_pos = reader.Position;

			Unknown = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Unknown), Unknown, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
