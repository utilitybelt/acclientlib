// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/MessageTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Messages.S2C {
	/// <summary>
	/// Add or update a dat file Resource.
	/// </summary>
	[Description("Add or update a dat file Resource.")]
	public partial class DDD_InterrogationMessage : IS2CMessage {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <inheritdoc />
		public S2CMessage OpCode => S2CMessage.DDD_InterrogationMessage;

		public uint ServersRegion { get; set; }

		public uint NameRuleLanguage { get; set; }

		public uint ProductId { get; set; }

		public PackableList<uint> SupportedLanguages { get; set; }

		public DDD_InterrogationMessage(IACProtocolReader reader) {
			ServersRegion = reader.ReadUInt32();

			NameRuleLanguage = reader.ReadUInt32();

			ProductId = reader.ReadUInt32();

			SupportedLanguages = new(reader);

		}
		public DDD_InterrogationMessage(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			ServersRegion = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ServersRegion), ServersRegion, reader, reader.Position - _pos);
			_pos = reader.Position;

			NameRuleLanguage = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(NameRuleLanguage), NameRuleLanguage, reader, reader.Position - _pos);
			_pos = reader.Position;

			ProductId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ProductId), ProductId, reader, reader.Position - _pos);
			_pos = reader.Position;

			SupportedLanguages = new(reader, new ParsedFieldInfo(ParseInfo, nameof(SupportedLanguages), SupportedLanguages, reader));
			ParseInfo?.TrackField(ParseInfo, SupportedLanguages.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
