// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/MessageTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Messages.S2C {
	/// <summary>
	/// Sets the parent for an object, eg. equipting an object.
	/// </summary>
	[Description("Sets the parent for an object, eg. equipting an object.")]
	public partial class Item_ParentEvent : IS2CMessage {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <inheritdoc />
		public S2CMessage OpCode => S2CMessage.Item_ParentEvent;

		/// <summary>
		/// id of the parent object
		/// </summary>
		[Description("id of the parent object")]
		public ObjectId ParentId { get; set; }

		/// <summary>
		/// id of the child object
		/// </summary>
		[Description("id of the child object")]
		public ObjectId ChildId { get; set; }

		/// <summary>
		/// Location object is being equipt to (Read from CSetup table in dat)
		/// </summary>
		[Description("Location object is being equipt to (Read from CSetup table in dat)")]
		public ParentLocation Location { get; set; }

		/// <summary>
		/// Placement frame id
		/// </summary>
		[Description("Placement frame id")]
		public Placement Placement { get; set; }

		/// <summary>
		/// The instance sequence value for the parent object (number of logins for players)
		/// </summary>
		[Description("The instance sequence value for the parent object (number of logins for players)")]
		public ushort ObjectInstanceSequence { get; set; }

		/// <summary>
		/// The position sequence value for the child object
		/// </summary>
		[Description("The position sequence value for the child object")]
		public ushort ChildPositionSequence { get; set; }

		public Item_ParentEvent(IACProtocolReader reader) {
			ParentId = reader.ReadUInt32();

			ChildId = reader.ReadUInt32();

			Location = (ParentLocation)reader.ReadUInt32();

			Placement = (Placement)reader.ReadUInt32();

			ObjectInstanceSequence = reader.ReadUInt16();

			ChildPositionSequence = reader.ReadUInt16();

		}
		public Item_ParentEvent(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			ParentId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ParentId), ParentId, reader, reader.Position - _pos);
			_pos = reader.Position;

			ChildId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(ChildId), ChildId, reader, reader.Position - _pos);
			_pos = reader.Position;

			Location = (ParentLocation)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Location), Location, reader, reader.Position - _pos);
			_pos = reader.Position;

			Placement = (Placement)reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(Placement), Placement, reader, reader.Position - _pos);
			_pos = reader.Position;

			ObjectInstanceSequence = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(ObjectInstanceSequence), ObjectInstanceSequence, reader, reader.Position - _pos);
			_pos = reader.Position;

			ChildPositionSequence = reader.ReadUInt16();
			ParseInfo?.TrackField(ParseInfo, nameof(ChildPositionSequence), ChildPositionSequence, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
