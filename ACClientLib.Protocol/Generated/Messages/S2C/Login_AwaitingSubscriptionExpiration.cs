// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/MessageTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Protocol.Messages.S2C {
	/// <summary>
	/// Sent when your subsciption is about to expire
	/// </summary>
	[Description("Sent when your subsciption is about to expire")]
	public partial class Login_AwaitingSubscriptionExpiration : IS2CMessage {
		public ParsedFieldInfo? ParseInfo { get; set; }

		/// <inheritdoc />
		public S2CMessage OpCode => S2CMessage.Login_AwaitingSubscriptionExpiration;

		/// <summary>
		/// Time remaining before your subscription expires.
		/// </summary>
		[Description("Time remaining before your subscription expires.")]
		public uint SecondsRemaining { get; set; }

		public Login_AwaitingSubscriptionExpiration(IACProtocolReader reader) {
			SecondsRemaining = reader.ReadUInt32();

		}
		public Login_AwaitingSubscriptionExpiration(IACProtocolReader reader, ParsedFieldInfo parseInfo) {
			ParseInfo = parseInfo;
			var _pos = reader.Position;

			SecondsRemaining = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(SecondsRemaining), SecondsRemaining, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
