// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// Sends all contract data
	/// </summary>
	[Description("Sends all contract data")]
	public partial class Social_SendClientContractTrackerTable : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Social_SendClientContractTrackerTable;

		public ContractTrackerTable ContractTracker { get; set; }

		public Social_SendClientContractTrackerTable(IACProtocolReader reader) : base(reader) {
			ContractTracker = new(reader);

		}
		public Social_SendClientContractTrackerTable(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			ContractTracker = new(reader, new ParsedFieldInfo(ParseInfo, nameof(ContractTracker), ContractTracker, reader));
			ParseInfo?.TrackField(ParseInfo, ContractTracker.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
