// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// ClearTradeAcceptance: Failure to complete a trade
	/// </summary>
	[Description("ClearTradeAcceptance: Failure to complete a trade")]
	public partial class Trade_ClearTradeAcceptance : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Trade_ClearTradeAcceptance;

		public Trade_ClearTradeAcceptance(IACProtocolReader reader) : base(reader) {
		}
		public Trade_ClearTradeAcceptance(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

		}
	}
}
