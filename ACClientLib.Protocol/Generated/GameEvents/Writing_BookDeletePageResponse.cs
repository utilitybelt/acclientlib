// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// Response to an attempt to delete a page from a book.
	/// </summary>
	[Description("Response to an attempt to delete a page from a book.")]
	public partial class Writing_BookDeletePageResponse : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Writing_BookDeletePageResponse;

		/// <summary>
		/// The readable object being changed.
		/// </summary>
		[Description("The readable object being changed.")]
		public ObjectId BookId { get; set; }

		/// <summary>
		/// The number the of page being deleted in the book.
		/// </summary>
		[Description("The number the of page being deleted in the book.")]
		public uint PageNumber { get; set; }

		/// <summary>
		/// Whether the request was successful
		/// </summary>
		[Description("Whether the request was successful")]
		public bool Success { get; set; }

		public Writing_BookDeletePageResponse(IACProtocolReader reader) : base(reader) {
			BookId = reader.ReadUInt32();

			PageNumber = reader.ReadUInt32();

			Success = reader.ReadBool();

		}
		public Writing_BookDeletePageResponse(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			BookId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(BookId), BookId, reader, reader.Position - _pos);
			_pos = reader.Position;

			PageNumber = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(PageNumber), PageNumber, reader, reader.Position - _pos);
			_pos = reader.Position;

			Success = reader.ReadBool();
			ParseInfo?.TrackField(ParseInfo, nameof(Success), Success, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
