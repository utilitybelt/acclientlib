// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// TODO: QueryPluginResponse
	/// </summary>
	[Description("TODO: QueryPluginResponse")]
	public partial class Admin_QueryPluginResponse2 : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Admin_QueryPluginResponse2;

		public Admin_QueryPluginResponse2(IACProtocolReader reader) : base(reader) {
		}
		public Admin_QueryPluginResponse2(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

		}
	}
}
