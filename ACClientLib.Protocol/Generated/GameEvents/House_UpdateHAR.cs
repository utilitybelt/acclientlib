// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// House Guest List
	/// </summary>
	[Description("House Guest List")]
	public partial class House_UpdateHAR : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.House_UpdateHAR;

		/// <summary>
		/// Set of house access records
		/// </summary>
		[Description("Set of house access records")]
		public HAR GuestList { get; set; }

		public House_UpdateHAR(IACProtocolReader reader) : base(reader) {
			GuestList = new(reader);

		}
		public House_UpdateHAR(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			GuestList = new(reader, new ParsedFieldInfo(ParseInfo, nameof(GuestList), GuestList, reader));
			ParseInfo?.TrackField(ParseInfo, GuestList.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
