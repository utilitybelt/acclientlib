// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// House Data
	/// </summary>
	[Description("House Data")]
	public partial class House_HouseStatus : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.House_HouseStatus;

		/// <summary>
		/// Type of message to display
		/// </summary>
		[Description("Type of message to display")]
		public uint NoticeType { get; set; }

		public House_HouseStatus(IACProtocolReader reader) : base(reader) {
			NoticeType = reader.ReadUInt32();

		}
		public House_HouseStatus(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			NoticeType = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(NoticeType), NoticeType, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
