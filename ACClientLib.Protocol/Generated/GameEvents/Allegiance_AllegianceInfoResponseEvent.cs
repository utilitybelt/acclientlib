// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// Returns data for a player's allegiance information
	/// </summary>
	[Description("Returns data for a player's allegiance information")]
	public partial class Allegiance_AllegianceInfoResponseEvent : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Allegiance_AllegianceInfoResponseEvent;

		/// <summary>
		/// Target of the request
		/// </summary>
		[Description("Target of the request")]
		public ObjectId TargetId { get; set; }

		/// <summary>
		/// Allegiance Profile Data
		/// </summary>
		[Description("Allegiance Profile Data")]
		public AllegianceProfile Profile { get; set; }

		public Allegiance_AllegianceInfoResponseEvent(IACProtocolReader reader) : base(reader) {
			TargetId = reader.ReadUInt32();

			Profile = new(reader);

		}
		public Allegiance_AllegianceInfoResponseEvent(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			TargetId = reader.ReadUInt32();
			ParseInfo?.TrackField(ParseInfo, nameof(TargetId), TargetId, reader, reader.Position - _pos);
			_pos = reader.Position;

			Profile = new(reader, new ParsedFieldInfo(ParseInfo, nameof(Profile), Profile, reader));
			ParseInfo?.TrackField(ParseInfo, Profile.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
