// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// Get Inscription Response, doesn't seem to be really used by client
	/// </summary>
	[Description("Get Inscription Response, doesn't seem to be really used by client")]
	public partial class Item_GetInscriptionResponse : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Item_GetInscriptionResponse;

		/// <summary>
		/// The inscription comment
		/// </summary>
		[Description("The inscription comment")]
		public string Inscription { get; set; }

		/// <summary>
		/// The name of the inscription author.
		/// </summary>
		[Description("The name of the inscription author.")]
		public string ScribeName { get; set; }

		public string ScribeAccount { get; set; }

		public Item_GetInscriptionResponse(IACProtocolReader reader) : base(reader) {
			Inscription = reader.ReadString();

			ScribeName = reader.ReadString();

			ScribeAccount = reader.ReadString();

		}
		public Item_GetInscriptionResponse(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			Inscription = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(Inscription), Inscription, reader, reader.Position - _pos);
			_pos = reader.Position;

			ScribeName = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(ScribeName), ScribeName, reader, reader.Position - _pos);
			_pos = reader.Position;

			ScribeAccount = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(ScribeAccount), ScribeAccount, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
