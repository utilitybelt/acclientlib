// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// HandleEvasionAttackerNotificationEvent: Your target has evaded your melee attack.
	/// </summary>
	[Description("HandleEvasionAttackerNotificationEvent: Your target has evaded your melee attack.")]
	public partial class Combat_HandleEvasionAttackerNotificationEvent : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Combat_HandleEvasionAttackerNotificationEvent;

		/// <summary>
		/// the name of your target
		/// </summary>
		[Description("the name of your target")]
		public string DefenderName { get; set; }

		public Combat_HandleEvasionAttackerNotificationEvent(IACProtocolReader reader) : base(reader) {
			DefenderName = reader.ReadString();

		}
		public Combat_HandleEvasionAttackerNotificationEvent(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			DefenderName = reader.ReadString();
			ParseInfo?.TrackField(ParseInfo, nameof(DefenderName), DefenderName, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
