// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// Fellow stats are done
	/// </summary>
	[Description("Fellow stats are done")]
	public partial class Fellowship_FellowStatsDone : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Fellowship_FellowStatsDone;

		public Fellowship_FellowStatsDone(IACProtocolReader reader) : base(reader) {
		}
		public Fellowship_FellowStatsDone(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

		}
	}
}
