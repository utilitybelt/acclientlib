// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/GameEventTypes.tt template file and regenerate

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Numerics;
using ACClientLib.Enums;
using ACClientLib.Protocol.Lib;
using ACClientLib.Protocol.Types;
using ACClientLib.Lib.Attributes;
using ACClientLib.Protocol.Messages.S2C;
namespace ACClientLib.Protocol.GameEvents {
	/// <summary>
	/// Updates a contract data
	/// </summary>
	[Description("Updates a contract data")]
	public partial class Social_SendClientContractTracker : Ordered_GameEvent, IGameEvent {
		/// <inheritdoc />
		public GameEvent EventOpCode => GameEvent.Social_SendClientContractTracker;

		public ContractTracker ContractTracker { get; set; }

		public bool DeleteContract { get; set; }

		public bool SetAsDisplayContract { get; set; }

		public Social_SendClientContractTracker(IACProtocolReader reader) : base(reader) {
			ContractTracker = new(reader);

			DeleteContract = reader.ReadBool();

			SetAsDisplayContract = reader.ReadBool();

		}
		public Social_SendClientContractTracker(IACProtocolReader reader, ParsedFieldInfo parseInfo) : base(reader, parseInfo) {
			var _pos = reader.Position;

			ContractTracker = new(reader, new ParsedFieldInfo(ParseInfo, nameof(ContractTracker), ContractTracker, reader));
			ParseInfo?.TrackField(ParseInfo, ContractTracker.ParseInfo, reader.Position - _pos);
			_pos = reader.Position;

			DeleteContract = reader.ReadBool();
			ParseInfo?.TrackField(ParseInfo, nameof(DeleteContract), DeleteContract, reader, reader.Position - _pos);
			_pos = reader.Position;

			SetAsDisplayContract = reader.ReadBool();
			ParseInfo?.TrackField(ParseInfo, nameof(SetAsDisplayContract), SetAsDisplayContract, reader, reader.Position - _pos);
			_pos = reader.Position;

		}
	}
}
