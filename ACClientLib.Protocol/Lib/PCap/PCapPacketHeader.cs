﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ACClientLib.Protocol.Lib.PCap {
	public record PCapPacketHeader {
		public PhysicalAddress EthDestination { get; }
		public PhysicalAddress EthSource { get; }
		public ushort EthProtocol { get; }

		public IPAddress SourceAddr { get; }
		public IPAddress DestinationAddr { get; }
		public byte IPProtocol { get; }

		public ushort SourcePort { get; }
		public ushort DestinationPort { get; }
		public ushort Length { get; }
		public ushort Crc { get; }

		public PCapPacketHeader() {

		}

		public PCapPacketHeader(IACProtocolReader reader) : this() {
			// ethernet frame
			reader.Position += 12;
			//EthDestination = new PhysicalAddress(reader.ReadBytes(6));
			//EthSource = new PhysicalAddress(reader.ReadBytes(6));
			EthProtocol = SwapBytes(reader.ReadUInt16());

            // ip header... 
            reader.Position += 9;
			IPProtocol = reader.ReadByte();
            reader.Position += 2;
			SourceAddr = new IPAddress(reader.ReadBytes(4));
			DestinationAddr = new IPAddress(reader.ReadBytes(4));

			// udp
			if (IPProtocol == 0x11) {
				SourcePort = SwapBytes(reader.ReadUInt16());
				DestinationPort = SwapBytes(reader.ReadUInt16());
				Length = SwapBytes(reader.ReadUInt16());
				Crc = SwapBytes(reader.ReadUInt16());
			}
		}

        public static ushort SwapBytes(ushort x) {
            return (ushort)((ushort)((x & 0xff) << 8) | ((x >> 8) & 0xff));
        }

        public static uint SwapBytes(uint x) {
            return ((x & 0x000000ff) << 24) +
                   ((x & 0x0000ff00) << 8) +
                   ((x & 0x00ff0000) >> 8) +
                   ((x & 0xff000000) >> 24);
        }
    }
}
