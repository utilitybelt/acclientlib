﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using ACClientLib.Protocol.Packets;

namespace ACClientLib.Protocol.Lib.PCap {
	public class PCapPacket {
		public PCapPacketHeader Header { get; }
        public uint Length { get; }
        public DateTime Timestamp { get; set; }
		public bool IsC2S => Header.SourcePort == 12345;

		public PCapPacket(DateTime utcNow, IACProtocolReader reader, uint capturedLength) {
			Header = new PCapPacketHeader(reader);
            Length = capturedLength;
		}
	}
}