﻿using ACClientLib.Protocol.Types;
using System;

namespace ACClientLib.Protocol.Lib {
	public class ACFragment {
        private const int MAX_FRAG_SIZE = 448;
        public Memory<byte> Data;
		public int Count;
		public int Received;
        public int Length;

        public uint Sequence;

		private bool[] chunks;

		public ACFragment(uint sequence, int count) {
			Sequence = sequence;
			Data = new byte[count * ACFragment.MAX_FRAG_SIZE];
			Count = count;

			chunks = new bool[Count];
		}

        internal void AddChunk(BlobFragments fragments) {
            if (fragments.Index < chunks.Length && !chunks[fragments.Index]) {
				fragments.Data.CopyTo(Data.Slice(fragments.Index * MAX_FRAG_SIZE, fragments.Data.Length));
                Received++;
                Length += fragments.Data.Length;
                chunks[fragments.Index] = true;
            }
            else {
                Console.WriteLine($"BlobFragments {fragments.Index}/{Count} ?? {fragments.Size} // {fragments.Sequence}");
            }
        }
    }
}
