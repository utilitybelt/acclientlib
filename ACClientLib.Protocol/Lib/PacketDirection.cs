﻿namespace ACClientLib.Protocol.Lib {
    public enum PacketDirection {
        C2S,
        S2C
    }
}