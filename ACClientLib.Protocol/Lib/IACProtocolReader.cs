﻿using System;
using System.IO;
using System.Numerics;

namespace ACClientLib.Protocol.Lib {
    public interface IACProtocolReader {
        Memory<byte> Data { get; set; }
        int Position { get; set; }
        int Length { get; set; }

        T? ReadACDataType<T>(ParsedFieldInfo? parseInfo = null);
        bool ReadBool();
        byte ReadByte();
        double ReadDouble();
        short ReadInt16();
        int ReadInt32();
        long ReadInt64();
        uint ReadPackedDWORD();
        short ReadPackedWORD();
        float ReadSingle();
        string ReadString();
        ushort ReadUInt16();
        uint ReadUInt32();
        ulong ReadUInt64();
        string ReadWString();
        Vector3 ReadVector3();
        Quaternion ReadQuaternion();
        byte[] ReadBytes(int count);
    }
}