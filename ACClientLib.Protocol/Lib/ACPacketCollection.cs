﻿using ACClientLib.Enums;
using ACClientLib.Protocol.Lib.PCap;
using ACClientLib.Protocol.Packets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACClientLib.Protocol.Lib {
    public class ACPacketCollection {
        public List<ACPCapPacket> Packets { get; } = new List<ACPCapPacket>();
        public Dictionary<uint, ACFragment> PendingFragments = new Dictionary<uint, ACFragment>();

        private ProtoManager _protoManager;

        public static async Task<ACPacketCollection> FromPCap(Stream stream, Action<string>? onProgress = null) {
            var collection = new ACPacketCollection();
            var pcapHeaderSize = 24;
            var buffer = new byte[pcapHeaderSize];
            var readBytes = await stream.ReadAsync(buffer, 0, pcapHeaderSize);
            var magic = BitConverter.ToUInt32(buffer, 0);

            if (readBytes != pcapHeaderSize || magic != 0xa1b2c3d4) {
                throw new Exception($"Unsupported PCap file. Magic was {magic}");
            }

            // ehhhhhh
            var readBuffer = new byte[stream.Length];
            //Console.WriteLine($"Buffer size is: {stream.Length - 24}");
            var offset = 0;
            int bytesRead;
            do {
                bytesRead = await stream.ReadAsync(readBuffer, pcapHeaderSize + offset, Math.Min(1024 * 1024, (int)stream.Length - pcapHeaderSize - offset));
                offset += bytesRead;
                onProgress?.Invoke($"Reading PCap file... {Math.Round(offset / (double)stream.Length * 100)}%");
                await Task.Delay(1);
            }
            while (bytesRead > 0);

            var mem = new Memory<byte>(readBuffer);
            var reader = new ACBinaryReader(mem);
            reader.Position += pcapHeaderSize;
            var parseInfo = new ParsedFieldInfo(null, "Root", collection, reader);

            var i = 0;
            var time = DateTime.UtcNow;
            var lastPosition = reader.Position;
            while (reader.Position < reader.Length) {
                lastPosition = reader.Position;
                try {
                    collection.ParsePCapPacket(reader, parseInfo);
                    if (i++ % 1000 == 0) {
                        onProgress?.Invoke($"Parsing PCap Packets... {Math.Round(reader.Position / (double)reader.Length * 100)}%");
                        await Task.Delay(1);
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                }

                if (lastPosition == reader.Position) {
                    Console.WriteLine($"Failing to read anything... bailing");
                    break;
                }
            }

            onProgress?.Invoke($"Finished!");
            return collection;
        }

        private void ParsePCapPacket(ACBinaryReader reader, ParsedFieldInfo? parseInfo = null) {
            var timestampSeconds = reader.ReadUInt32();
            var timestampMicroseconds = reader.ReadUInt32();
            var capturedLength = reader.ReadUInt32();
            var actualLength = reader.ReadUInt32();

            var pcapPacket = new PCapPacket(DateTime.UtcNow, reader, capturedLength);

            var start = reader.Position;
            var packetParseInfo = parseInfo is null ? null : new ParsedFieldInfo(parseInfo, "PCapPacket", null, reader);
            AddPCapPacket(pcapPacket, reader, packetParseInfo);

            if (reader.Position < reader.Length) {
                Console.WriteLine($"Packet reader ended early... {reader.Position} / {reader.Length} (next is: {string.Join(" ", reader.Data.Slice(reader.Position, reader.Length).ToArray().Select(b => $"{b:X2}"))})");
            }

            reader.Position = (int)(start + capturedLength);
        }

        public ACPacketCollection() {
            _protoManager = new ProtoManager();
        }

        int i = 0;
        public void AddPCapPacket(PCapPacket packet, ACBinaryReader reader, ParsedFieldInfo? parseInfo = null) {
            try {
                var acPackets = new List<IACPacket>();
                if (packet.IsC2S) {
                    acPackets.AddRange(_protoManager.HandleC2SData(reader, (int)packet.Length, parseInfo));
                }
                else {
                    acPackets.AddRange(_protoManager.HandleS2CData(reader, (int)packet.Length, parseInfo));
                }

                foreach (var acPacket in acPackets) {
                    Packets.Add(new ACPCapPacket(packet, acPacket));
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
