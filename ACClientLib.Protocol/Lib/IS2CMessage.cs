﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public interface IS2CMessage : IACMessage {
        public S2CMessage OpCode { get; }
    }
}
