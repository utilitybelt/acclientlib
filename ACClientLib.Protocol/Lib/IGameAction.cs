﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public interface IGameAction {
        public GameAction ActionOpCode { get; }
    }
}
