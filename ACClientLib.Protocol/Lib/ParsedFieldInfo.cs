﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public class ParsedFieldInfo {
        public List<ParsedFieldInfo> Children { get; } = [];
        public int Offset { get; set; }
        public int Length { get; set; }
        public object? Value { get; set; }
        public ParsedFieldInfo? Parent { get; set; }
        public string Name { get; set; }

        readonly IACProtocolReader _reader;

        public ParsedFieldInfo(ParsedFieldInfo? parent, string name, object? value, IACProtocolReader reader, int length = 0) {
            Parent = parent;
            Name = name;
            Offset = reader.Position - length;
            Length = length;
            Value = value;
            _reader = reader;
        }

        public void TrackField(ParsedFieldInfo? parent, string name, object value, IACProtocolReader reader, int length) {
            Value ??= parent;
            Children.Add(new ParsedFieldInfo(parent, name, value, reader, length));
            Length += length;
        }

        public void TrackField(ParsedFieldInfo? parent, ParsedFieldInfo? child, int length) {
            Value ??= parent;
            if (child is not null) {
                Children.Add(child);
            }
            Length += length;
        }

        public override string ToString() {
            return ToString(0).ToString();
        }

        public StringBuilder ToString(int depth, StringBuilder? str = null) {
            str ??= new StringBuilder();

            var p = (Value?.GetType().IsPrimitive != true && Value?.GetType().IsEnum != true ? Value?.GetType().ToString() : Value.ToString());
            var bin = $"{string.Join(" ", _reader.Data.Slice(Offset, Length).ToArray().Select(b => $"{b:X2}"))}";
            str.AppendLine($"{new string('\t', depth)}{Name}({Value?.GetType().Name}): {p} @ 0x{Offset:X}(r0x{Offset - Parent?.Offset:X}):{Length:X} ({bin})");
            foreach (var child in Children) {
                child.ToString(depth + 1, str);
            }

            return str;
        }
    }
}
