﻿using ACClientLib.Enums;
using ACClientLib.Protocol.Types;
using System.Collections.Generic;
using System.ComponentModel;

namespace ACClientLib.Protocol.Lib {
    public interface IACPacket : IACDataType {
        /// <summary>
        /// The direction of this packet. Either server to client, or client to server.
        /// </summary>
        [Description("The direction of this packet. Either server to client, or client to server.")]
        public PacketDirection Direction { get; }

        /// <summary>
        /// Packet Checksum
        /// </summary>
        [Description("Packet Checksum")]
        public uint Checksum { get; set; }

        /// <summary>
        /// Flags that dictate the content / purpose of this packet
        /// </summary>
        [Description("Flags that dictate the content / purpose of this packet")]
        public PacketHeaderFlags Flags { get; set; }

        public ushort Iteration { get; set; }

        /// <summary>
        /// Packet Sequence / Order
        /// </summary>
        [Description("Packet Sequence / Order")]
        public uint Sequence { get; set; }

        /// <summary>
        /// Packet length, excluding this header
        /// </summary>
        [Description("Packet length, excluding this header")]
        public ushort Size { get; set; }

        public ushort TimeSinceLastPacket { get; set; }

        public ushort RecipientId { get; set; }

        /// <summary>
        /// Used to contruct messages from potentially multple fragments. Only valid if packet header has BlobFragments flag
        /// </summary>
        [Description("Used to contruct messages from potentially multple fragments. Only valid if packet header has BlobFragments flag")]
        public BlobFragments? Fragments { get; set; }

        /// <summary>
        /// A list of any messages that got finished building from fragment data after this packet was received.
        /// </summary>
        [Description("A list of any messages that got finished building from fragment data after this packet was received.")]
        public List<IACMessage>? Messages { get; set; }
    }
}