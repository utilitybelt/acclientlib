﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public class C2SMessageEventArgs {
        public C2SMessage Type;
        public IC2SMessage Message;

        public C2SMessageEventArgs(C2SMessage type, IC2SMessage message) {
            Type = type;
            Message = message;
        }
    }
}
