﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public class UnknownC2SMessageEventArgs {
        public C2SMessage Type;
        public byte[] Data;

        public UnknownC2SMessageEventArgs(C2SMessage type, byte[] rawData) {
            Type = type;
            Data = rawData;
        }
    }
}
