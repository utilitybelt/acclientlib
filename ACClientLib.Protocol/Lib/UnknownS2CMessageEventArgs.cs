﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public class UnknownS2CMessageEventArgs {
        public S2CMessage Type;
        public byte[] Data;

        public UnknownS2CMessageEventArgs(S2CMessage type, byte[] rawData) {
            Type = type;
            Data = rawData;
        }
    }
}
