﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public interface IACDataType {
        public ParsedFieldInfo? ParseInfo { get; set; }
    }
}
