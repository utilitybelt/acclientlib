﻿using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Linq;

namespace ACClientLib.Protocol.Lib {
    /// <summary>
    /// A collection of helper methods for reading AC network message binary streams
    /// </summary>
    public class ACBinaryReader : IACProtocolReader {
        public int Position { get; set; } = 0;
        public int Length { get; set; } = 0;
        public Memory<byte> Data { get; set; }

        public ACBinaryReader(Memory<byte> data) {
            Data = data;
            Position = 0;
            Length = data.Length;
        }

        /// <summary>
        /// Read an item of type T from the buffer and return its value
        /// </summary>
        /// <typeparam name="T">type of item to read</typeparam>
        /// <param name="buffer">buffer to read from</param>
        /// <returns></returns>
        public T? ReadACDataType<T>(ParsedFieldInfo? parseInfo = null) {
            if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                return (T)Activator.CreateInstance(typeof(T), [this, parseInfo]);
            }
            else {
                var type = typeof(T).IsEnum ? Enum.GetUnderlyingType(typeof(T)) : typeof(T);

                if (type == typeof(ushort))
                    return (T)(object)ReadUInt16();
                else if (type == typeof(short))
                    return (T)(object)ReadInt16();
                else if (type == typeof(uint))
                    return (T)(object)ReadUInt32();
                else if (type == typeof(int))
                    return (T)(object)ReadInt32();
                else if (type == typeof(ulong))
                    return (T)(object)ReadUInt64();
                else if (type == typeof(long))
                    return (T)(object)ReadInt64();
                else if (type == typeof(float))
                    return (T)(object)ReadSingle();
                else if (type == typeof(double))
                    return (T)(object)ReadDouble();
                else if (type == typeof(byte))
                    return (T)(object)ReadByte();
                else if (type == typeof(bool))
                    return (T)(object)ReadBool();
                else if (type == typeof(string))
                    return (T)(object)ReadString();
                else {
                    return default;
                }
            }
        }

        /// <summary>
        /// Read a UInt16 from the buffer and advance the position
        /// </summary>
        public ushort ReadUInt16() {
            var val = BinaryPrimitives.ReadUInt16LittleEndian(Data.Slice(Position, sizeof(ushort)).Span);
            Position += sizeof(ushort);
            return val;
        }

        /// <summary>
        /// Read a Int16 from the buffer and advance the position
        /// </summary>
        public short ReadInt16() {
            var val = BinaryPrimitives.ReadInt16LittleEndian(Data.Slice(Position, sizeof(short)).Span);
            Position += sizeof(short);
            return val;
        }

        /// <summary>
        /// Read a UInt32 from the buffer and advance the position
        /// </summary>
        public uint ReadUInt32() {
            var val = BinaryPrimitives.ReadUInt32LittleEndian(Data.Slice(Position, sizeof(uint)).Span);
            Position += sizeof(uint);
            return val;
        }

        /// <summary>
        /// Read a Int32 from the buffer and advance the position
        /// </summary>
        public int ReadInt32() {
            var val = BinaryPrimitives.ReadInt32LittleEndian(Data.Slice(Position, sizeof(int)).Span);
            Position += sizeof(int);
            return val;
        }

        /// <summary>
        /// Read a UInt64 from the buffer and advance the position
        /// </summary>
        public ulong ReadUInt64() {
            var val = BinaryPrimitives.ReadUInt64LittleEndian(Data.Slice(Position, sizeof(ulong)).Span);
            Position += sizeof(ulong);
            return val;
        }

        /// <summary>
        /// Read a Int64 from the buffer and advance the position
        /// </summary>
        public long ReadInt64() {
            var val = MemoryMarshal.Read<long>(Data.Slice(Position, sizeof(long)).Span);
            Position += sizeof(long);
            return val;
        }

        /// <summary>
        /// Read a Single (float) from the buffer and advance the position
        /// </summary>
        public float ReadSingle() {
            var val = MemoryMarshal.Read<float>(Data.Slice(Position, sizeof(float)).Span);
            Position += sizeof(float);
            return val;
        }

        /// <summary>
        /// Read a Double from the buffer and advance the position
        /// </summary>
        public double ReadDouble() {
            var val = MemoryMarshal.Read<double>(Data.Slice(Position, sizeof(double)).Span);
            Position += sizeof(double);
            return val;
        }

        /// <summary>
        /// Read a Bool from the buffer and advance the position
        /// </summary>
        public bool ReadBool() {
            var val = ReadInt32();
            return val == 1;
        }

        /// <summary>
        /// Read a Byte from the buffer and advance the position
        /// </summary>
        public byte ReadByte() {
            var val = MemoryMarshal.Read<byte>(Data.Slice(Position, 1).Span);
            Position += 1;
            return val;
        }

        /// <summary>
        /// Read a PackedWORD from the buffer and advance the position
        /// </summary>
        public short ReadPackedWORD() {
            var tmp = (short)ReadByte();
            if ((tmp & 0x80) != 0)
                tmp = (short)(((tmp & 0x7f) << 8) | ReadByte());

            return tmp;
        }

        /// <summary>
        /// Read a PackedWORD from the buffer and advance the position
        /// </summary>
        public uint ReadPackedDWORD() {
            int tmp = (int)ReadInt16();
            if ((tmp & 0x8000) != 0)
                tmp = (short)(((tmp & 0x7fff) << 8) | (int)ReadInt16());

            return (uint)tmp;
        }

        /// <summary>
        /// Read a ascii string from the buffer and advance the position, aligning to 4 bytes afterwards
        /// </summary>
        public string ReadString() {
            int start = Position;
            int length = ReadInt16();
            if (length == -1) {
                length = ReadInt32();
            }

            var tmp = ReadBytes(length);
            int align = (int)((Position - start) % 4);
            if (align > 0)
                _ = ReadBytes(4 - align);

            var val = Encoding.ASCII.GetString(tmp);
            return val;
        }

        /// <summary>
        /// Read a unicode string from the buffer and advance the position
        /// </summary>
        public string ReadWString() {
            int length = ReadByte();
            if ((length & 0x80) != 0) {
                length = ((length & 0x7f) << 8) | ReadByte();
            }

            var tmp = Data.Slice(Position, length).ToArray();
            var val = Encoding.Unicode.GetString(tmp);
            Position += length;

            return val;
        }

        public Vector3 ReadVector3() {
            var x = ReadSingle();
            var y = ReadSingle();
            var z = ReadSingle();
            return new Vector3(x, y, z);
        }

        public Quaternion ReadQuaternion() {
            var w = ReadSingle();
            var x = ReadSingle();
            var y = ReadSingle();
            var z = ReadSingle();
            return new Quaternion(x, y, z, w);
        }

        public byte[] ReadBytes(int count) {
            var bytes = Data.Slice(Position, count).ToArray();
            Position += count;
            return bytes;
        }
    }
}
