﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public interface IC2SMessage : IACMessage {
        public C2SMessage OpCode { get; }
    }
}
