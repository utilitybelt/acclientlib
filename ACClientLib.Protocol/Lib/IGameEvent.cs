﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public interface IGameEvent {
        public GameEvent EventOpCode { get; }
    }
}
