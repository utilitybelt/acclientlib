﻿using System;
using System.Collections.Generic;
using System.Text;
using ACClientLib.Protocol.Lib.PCap;
using ACClientLib.Protocol.Packets;

namespace ACClientLib.Protocol.Lib {
    public class ACPCapPacket {
        public PCapPacket PCapPacket { get; }

        public IACPacket ACPacket { get; }
		public int Id { get; internal set; }

		public ACPCapPacket(PCapPacket pcapPacket, IACPacket acPacket) {
            PCapPacket = pcapPacket;
            ACPacket = acPacket;
        }
    }
}
