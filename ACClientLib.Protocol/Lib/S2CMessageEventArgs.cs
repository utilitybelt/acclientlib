﻿using ACClientLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Protocol.Lib {
    public class S2CMessageEventArgs {
        public S2CMessage Type;
        public IS2CMessage Message;

        public S2CMessageEventArgs(S2CMessage type, IS2CMessage message) {
            Type = type;
            Message = message;
        }
    }
}
