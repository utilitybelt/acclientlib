﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Lib.Attributes {
    public class MappedEnumAttribute : Attribute {
        public Type Type { get; set; }

        public MappedEnumAttribute(Type type) {
            Type = type;
        }
    }
}
