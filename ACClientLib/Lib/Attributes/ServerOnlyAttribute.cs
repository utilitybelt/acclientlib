﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Lib.Attributes {
    public class ServerOnlyAttribute : Attribute {
        public bool HasOtherSource { get; }

        public ServerOnlyAttribute() {
        
        }

        public ServerOnlyAttribute(bool hasOtherSource) {
            HasOtherSource = hasOtherSource;
        }
    }
}
