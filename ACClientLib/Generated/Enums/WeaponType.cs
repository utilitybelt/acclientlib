// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	public enum WeaponType : uint {
		Undef = 0,
		Unarmed = 1,
		Sword = 2,
		Axe = 3,
		Mace = 4,
		Spear = 5,
		Dagger = 6,
		Staff = 7,
		Bow = 8,
		Crossbow = 9,
		Thrown = 10,
		TwoHanded = 11,
		Magic = 12,
	}
}
