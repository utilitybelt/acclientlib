// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	[Flags]
	public enum PlayerKillerStatus : int {
		Undef = 0x00,
		Protected = 0x01,
		NPK = 0x02,
		PK = 0x04,
		Unprotected = 0x08,
		RubberGlue = 0x10,
		Free = 0x20,
		PKLite = 0x40,
		Creature = Unprotected,
		Trap = Unprotected,
		NPC = Protected,
		Vendor = RubberGlue,
		Baelzharon = Free,
	}
}
