// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	/// <summary>
	/// The AttributeMask selects which creature attributes highlighting is applied to.
	/// </summary>
	[Description("The AttributeMask selects which creature attributes highlighting is applied to.")]
	[Flags]
	public enum AttributeMask : ushort {
		Strength = 0x0001,
		Endurance = 0x0002,
		Quickness = 0x0004,
		Coordination = 0x0008,
		Focus = 0x0010,
		Self = 0x0020,
		Health = 0x0040,
		Stamina = 0x0080,
		Mana = 0x0100,
	}
}
