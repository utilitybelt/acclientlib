// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	/// <summary>
	/// Heritage of a player
	/// </summary>
	[Description("Heritage of a player")]
	public enum HeritageGroup : byte {
		Invalid = 0x00,
		Aluvian = 0x01,
		Gharundim = 0x02,
		Sho = 0x03,
		Viamontian = 0x04,
		Shadowbound = 0x05,
		Gearknight = 0x06,
		Tumerok = 0x07,
		Lugian = 0x08,
		Empyrean = 0x09,
		Penumbraen = 0x0A,
		Undead = 0x0B,
		Olthoi = 0x0C,
		OlthoiAcid = 0x0D,
	}
}
