// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	/// <summary>
	/// The movement type defines the fields for the rest of the message
	/// </summary>
	[Description("The movement type defines the fields for the rest of the message")]
	public enum MovementType : byte {
		InterpertedMotionState = 0x00,
		MoveToObject = 0x06,
		MoveToPosition = 0x07,
		TurnToObject = 0x08,
		TurnToPosition = 0x09,
	}
}
