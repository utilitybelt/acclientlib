// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	public enum PortalBitmask : int {
		Undef = 0x00,
		NotPassable = 0x00,
		Unrestricted = 0x01,
		NoPk = 0x02,
		NoPKLite = 0x04,
		NoNPK = 0x08,
		NoSummon = 0x10,
		NoRecall = 0x20,
		OnlyOlthoiPCs = 0x40,
		NoOlthoiPCs = 0x80,
		NoVitae = 0x100,
		NoNewAccounts = 0x200,
	}
}
