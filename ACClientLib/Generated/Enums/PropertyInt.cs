// IMPORTANT:
// This file is AUTO GENERATED by a t4 template.
// DO NOT EDIT this directly, instead edit the Lib/SourceGen/ProtocolEnums.tt template file and regenerate

using System;
using System.ComponentModel;
using ACClientLib.Lib.Attributes;
namespace ACClientLib.Enums {
	/// <summary>
	/// The PropertyInt identifies a specific Character or Object int property.
	/// </summary>
	[Description("The PropertyInt identifies a specific Character or Object int property.")]
	public enum PropertyInt : uint {
		[ServerOnly]
		[MappedEnum(typeof(ItemType))]
		ItemType = 1,
		[MappedEnum(typeof(CreatureType))]
		CreatureType = 2,
		[ServerOnly]
		[MappedEnum(typeof(PaletteTemplate))]
		PaletteTemplate = 3,
		[MappedEnum(typeof(CoverageMask))]
		ClothingPriority = 4,
		[SendOnCreate]
		EncumbranceVal = 5,
		[SendOnCreate]
		ItemsCapacity = 6,
		[SendOnCreate]
		ContainersCapacity = 7,
		[ServerOnly]
		Mass = 8,
		[ServerOnly]
		[MappedEnum(typeof(EquipMask))]
		ValidLocations = 9,
		[ServerOnly]
		[MappedEnum(typeof(EquipMask))]
		CurrentWieldedLocation = 10,
		[ServerOnly]
		MaxStackSize = 11,
		[ServerOnly]
		StackSize = 12,
		[ServerOnly]
		StackUnitEncumbrance = 13,
		[ServerOnly]
		StackUnitMass = 14,
		[ServerOnly]
		StackUnitValue = 15,
		[ServerOnly]
		[MappedEnum(typeof(Usable))]
		ItemUseable = 16,
		RareId = 17,
		[ServerOnly]
		[MappedEnum(typeof(UiEffects))]
		UiEffects = 18,
		Value = 19,
		[SendOnCreate]
		CoinValue = 20,
		TotalExperience = 21,
		AvailableCharacter = 22,
		[SendOnCreate]
		TotalSkillCredits = 23,
		[SendOnCreate]
		AvailableSkillCredits = 24,
		[SendOnCreate]
		Level = 25,
		[MappedEnum(typeof(SubscriptionStatus))]
		AccountRequirements = 26,
		[MappedEnum(typeof(ArmorType))]
		ArmorType = 27,
		ArmorLevel = 28,
		AllegianceCpPool = 29,
		[SendOnCreate]
		AllegianceRank = 30,
		[MappedEnum(typeof(Channel))]
		ChannelsAllowed = 31,
		[MappedEnum(typeof(Channel))]
		ChannelsActive = 32,
		[MappedEnum(typeof(BondedStatus))]
		Bonded = 33,
		MonarchsRank = 34,
		AllegianceFollowers = 35,
		ResistMagic = 36,
		ResistItemAppraisal = 37,
		ResistLockpick = 38,
		DeprecatedResistRepair = 39,
		[SendOnCreate]
		[MappedEnum(typeof(CombatMode))]
		CombatMode = 40,
		[MappedEnum(typeof(AttackHeight))]
		CurrentAttackHeight = 41,
		CombatCollisions = 42,
		[SendOnCreate]
		NumDeaths = 43,
		Damage = 44,
		[MappedEnum(typeof(DamageType))]
		DamageType = 45,
		[ServerOnly]
		[MappedEnum(typeof(CombatStyle))]
		DefaultCombatStyle = 46,
		[SendOnCreate]
		[MappedEnum(typeof(AttackType))]
		AttackType = 47,
		[MappedEnum(typeof(SkillId))]
		WeaponSkill = 48,
		WeaponTime = 49,
		[MappedEnum(typeof(AmmoType))]
		AmmoType = 50,
		[MappedEnum(typeof(CombatUse))]
		CombatUse = 51,
		[ServerOnly]
		[MappedEnum(typeof(ParentLocation))]
		ParentLocation = 52,
		[ServerOnly]
		[MappedEnum(typeof(Placement))]
		PlacementPosition = 53,
		WeaponEncumbrance = 54,
		WeaponMass = 55,
		ShieldValue = 56,
		ShieldEncumbrance = 57,
		MissileInventoryLocation = 58,
		FullDamageType = 59,
		WeaponRange = 60,
		AttackersSkill = 61,
		DefendersSkill = 62,
		AttackersSkillValue = 63,
		AttackersClass = 64,
		[ServerOnly]
		[MappedEnum(typeof(Placement))]
		Placement = 65,
		CheckpointStatus = 66,
		Tolerance = 67,
		TargetingTactic = 68,
		CombatTactic = 69,
		HomesickTargetingTactic = 70,
		NumFollowFailures = 71,
		[MappedEnum(typeof(CreatureType))]
		FriendType = 72,
		[MappedEnum(typeof(CreatureType))]
		FoeType = 73,
		[MappedEnum(typeof(ItemType))]
		MerchandiseItemTypes = 74,
		MerchandiseMinValue = 75,
		MerchandiseMaxValue = 76,
		NumItemsSold = 77,
		NumItemsBought = 78,
		MoneyIncome = 79,
		MoneyOutflow = 80,
		MaxGeneratedObjects = 81,
		InitGeneratedObjects = 82,
		[MappedEnum(typeof(ActivationResponse))]
		ActivationResponse = 83,
		OriginalValue = 84,
		NumMoveFailures = 85,
		MinLevel = 86,
		MaxLevel = 87,
		LockpickMod = 88,
		BoosterEnum = 89,
		BoostValue = 90,
		MaxStructure = 91,
		Structure = 92,
		[ServerOnly]
		[MappedEnum(typeof(PhysicsState))]
		PhysicsState = 93,
		[ServerOnly]
		[MappedEnum(typeof(ItemType))]
		TargetType = 94,
		[MappedEnum(typeof(RadarColor))]
		RadarBlipColor = 95,
		EncumbranceCapacity = 96,
		LoginTimestamp = 97,
		CreationTimestamp = 98,
		PkLevelModifier = 99,
		[MappedEnum(typeof(GeneratorType))]
		GeneratorType = 100,
		[MappedEnum(typeof(CombatStyle))]
		AiAllowedCombatStyle = 101,
		LogoffTimestamp = 102,
		[MappedEnum(typeof(GeneratorDestruct))]
		GeneratorDestructionType = 103,
		ActivationCreateClass = 104,
		ItemWorkmanship = 105,
		ItemSpellcraft = 106,
		ItemCurMana = 107,
		ItemMaxMana = 108,
		ItemDifficulty = 109,
		ItemAllegianceRankLimit = 110,
		[MappedEnum(typeof(PortalBitmask))]
		PortalBitmask = 111,
		AdvocateLevel = 112,
		[SendOnCreate]
		[MappedEnum(typeof(Gender))]
		Gender = 113,
		[MappedEnum(typeof(AttunedStatus))]
		Attuned = 114,
		ItemSkillLevelLimit = 115,
		GateLogic = 116,
		ItemManaCost = 117,
		Logoff = 118,
		Active = 119,
		[MappedEnum(typeof(AttackHeight))]
		AttackHeight = 120,
		NumAttackFailures = 121,
		AiCpThreshold = 122,
		AiAdvancementStrategy = 123,
		Version = 124,
		[SendOnCreate]
		Age = 125,
		VendorHappyMean = 126,
		VendorHappyVariance = 127,
		CloakStatus = 128,
		[SendOnCreate]
		VitaeCpPool = 129,
		NumServicesSold = 130,
		[MappedEnum(typeof(MaterialType))]
		MaterialType = 131,
		[SendOnCreate]
		NumAllegianceBreaks = 132,
		[MappedEnum(typeof(RadarBehavior))]
		ShowableOnRadar = 133,
		[SendOnCreate]
		PlayerKillerStatus = 134,
		VendorHappyMaxItems = 135,
		ScorePageNum = 136,
		ScoreConfigNum = 137,
		ScoreNumScores = 138,
		[SendOnCreate]
		DeathLevel = 139,
		[ServerOnly]
		AiOptions = 140,
		[ServerOnly]
		OpenToEveryone = 141,
		[ServerOnly]
		[MappedEnum(typeof(GeneratorTimeType))]
		GeneratorTimeType = 142,
		[ServerOnly]
		GeneratorStartTime = 143,
		[ServerOnly]
		GeneratorEndTime = 144,
		[ServerOnly]
		[MappedEnum(typeof(GeneratorDestruct))]
		GeneratorEndDestructionType = 145,
		[ServerOnly]
		XpOverride = 146,
		NumCrashAndTurns = 147,
		ComponentWarningThreshold = 148,
		[MappedEnum(typeof(HouseStatus))]
		HouseStatus = 149,
		[ServerOnly]
		[MappedEnum(typeof(Placement))]
		HookPlacement = 150,
		[ServerOnly]
		[MappedEnum(typeof(HookType))]
		HookType = 151,
		[ServerOnly]
		[MappedEnum(typeof(ItemType))]
		HookItemType = 152,
		AiPpThreshold = 153,
		GeneratorVersion = 154,
		[MappedEnum(typeof(HouseType))]
		HouseType = 155,
		PickupEmoteOffset = 156,
		WeenieIteration = 157,
		[MappedEnum(typeof(WieldRequirement))]
		WieldRequirements = 158,
		[MappedEnum(typeof(SkillId))]
		WieldSkillType = 159,
		WieldDifficulty = 160,
		[ServerOnly]
		HouseMaxHooksUsable = 161,
		[ServerOnly]
		HouseCurrentHooksUsable = 162,
		AllegianceMinLevel = 163,
		AllegianceMaxLevel = 164,
		HouseRelinkHookCount = 165,
		[MappedEnum(typeof(CreatureType))]
		SlayerCreatureType = 166,
		ConfirmationInProgress = 167,
		ConfirmationTypeInProgress = 168,
		TsysMutationData = 169,
		NumItemsInMaterial = 170,
		NumTimesTinkered = 171,
		AppraisalLongDescDecoration = 172,
		AppraisalLockpickSuccessPercent = 173,
		AppraisalPages = 174,
		AppraisalMaxPages = 175,
		[MappedEnum(typeof(SkillId))]
		AppraisalItemSkill = 176,
		GemCount = 177,
		GemType = 178,
		[MappedEnum(typeof(ImbuedEffectType))]
		ImbuedEffect = 179,
		AttackersRawSkillValue = 180,
		[SendOnCreate]
		ChessRank = 181,
		ChessTotalGames = 182,
		ChessGamesWon = 183,
		ChessGamesLost = 184,
		TypeOfAlteration = 185,
		[MappedEnum(typeof(SkillId))]
		SkillToBeAltered = 186,
		SkillAlterationCount = 187,
		[SendOnCreate]
		[MappedEnum(typeof(HeritageGroup))]
		HeritageGroup = 188,
		TransferFromAttribute = 189,
		TransferToAttribute = 190,
		AttributeTransferCount = 191,
		[SendOnCreate]
		FakeFishingSkill = 192,
		NumKeys = 193,
		DeathTimestamp = 194,
		PkTimestamp = 195,
		VictimTimestamp = 196,
		[ServerOnly]
		[MappedEnum(typeof(HookGroupType))]
		HookGroup = 197,
		AllegianceSwearTimestamp = 198,
		[SendOnCreate]
		HousePurchaseTimestamp = 199,
		RedirectableEquippedArmorCount = 200,
		MeleeDefenseImbuedEffectTypeCache = 201,
		MissileDefenseImbuedEffectTypeCache = 202,
		MagicDefenseImbuedEffectTypeCache = 203,
		ElementalDamageBonus = 204,
		ImbueAttempts = 205,
		ImbueSuccesses = 206,
		CreatureKills = 207,
		PlayerKillsPk = 208,
		PlayerKillsPkl = 209,
		RaresTierOne = 210,
		RaresTierTwo = 211,
		RaresTierThree = 212,
		RaresTierFour = 213,
		RaresTierFive = 214,
		[SendOnCreate]
		AugmentationStat = 215,
		[SendOnCreate]
		AugmentationFamilyStat = 216,
		[SendOnCreate]
		AugmentationInnateFamily = 217,
		[SendOnCreate]
		AugmentationInnateStrength = 218,
		[SendOnCreate]
		AugmentationInnateEndurance = 219,
		[SendOnCreate]
		AugmentationInnateCoordination = 220,
		[SendOnCreate]
		AugmentationInnateQuickness = 221,
		[SendOnCreate]
		AugmentationInnateFocus = 222,
		[SendOnCreate]
		AugmentationInnateSelf = 223,
		[SendOnCreate]
		AugmentationSpecializeSalvaging = 224,
		[SendOnCreate]
		AugmentationSpecializeItemTinkering = 225,
		[SendOnCreate]
		AugmentationSpecializeArmorTinkering = 226,
		[SendOnCreate]
		AugmentationSpecializeMagicItemTinkering = 227,
		[SendOnCreate]
		AugmentationSpecializeWeaponTinkering = 228,
		[SendOnCreate]
		AugmentationExtraPackSlot = 229,
		[SendOnCreate]
		AugmentationIncreasedCarryingCapacity = 230,
		[SendOnCreate]
		AugmentationLessDeathItemLoss = 231,
		[SendOnCreate]
		AugmentationSpellsRemainPastDeath = 232,
		[SendOnCreate]
		AugmentationCriticalDefense = 233,
		[SendOnCreate]
		AugmentationBonusXp = 234,
		[SendOnCreate]
		AugmentationBonusSalvage = 235,
		[SendOnCreate]
		AugmentationBonusImbueChance = 236,
		[SendOnCreate]
		AugmentationFasterRegen = 237,
		[SendOnCreate]
		AugmentationIncreasedSpellDuration = 238,
		[SendOnCreate]
		AugmentationResistanceFamily = 239,
		[SendOnCreate]
		AugmentationResistanceSlash = 240,
		[SendOnCreate]
		AugmentationResistancePierce = 241,
		[SendOnCreate]
		AugmentationResistanceBlunt = 242,
		[SendOnCreate]
		AugmentationResistanceAcid = 243,
		[SendOnCreate]
		AugmentationResistanceFire = 244,
		[SendOnCreate]
		AugmentationResistanceFrost = 245,
		[SendOnCreate]
		AugmentationResistanceLightning = 246,
		RaresTierOneLogin = 247,
		RaresTierTwoLogin = 248,
		RaresTierThreeLogin = 249,
		RaresTierFourLogin = 250,
		RaresTierFiveLogin = 251,
		RaresLoginTimestamp = 252,
		RaresTierSix = 253,
		RaresTierSeven = 254,
		RaresTierSixLogin = 255,
		RaresTierSevenLogin = 256,
		ItemAttributeLimit = 257,
		ItemAttributeLevelLimit = 258,
		ItemAttribute2ndLimit = 259,
		ItemAttribute2ndLevelLimit = 260,
		CharacterTitleId = 261,
		NumCharacterTitles = 262,
		[MappedEnum(typeof(DamageType))]
		ResistanceModifierType = 263,
		FreeTinkersBitfield = 264,
		[MappedEnum(typeof(EquipmentSet))]
		EquipmentSetId = 265,
		PetClass = 266,
		Lifespan = 267,
		RemainingLifespan = 268,
		UseCreateQuantity = 269,
		[MappedEnum(typeof(WieldRequirement))]
		WieldRequirements2 = 270,
		[MappedEnum(typeof(SkillId))]
		WieldSkillType2 = 271,
		WieldDifficulty2 = 272,
		[MappedEnum(typeof(WieldRequirement))]
		WieldRequirements3 = 273,
		[MappedEnum(typeof(SkillId))]
		WieldSkillType3 = 274,
		WieldDifficulty3 = 275,
		[MappedEnum(typeof(WieldRequirement))]
		WieldRequirements4 = 276,
		[MappedEnum(typeof(SkillId))]
		WieldSkillType4 = 277,
		WieldDifficulty4 = 278,
		Unique = 279,
		SharedCooldown = 280,
		[SendOnCreate]
		[MappedEnum(typeof(FactionBits))]
		Faction1Bits = 281,
		[MappedEnum(typeof(FactionBits))]
		Faction2Bits = 282,
		[MappedEnum(typeof(FactionBits))]
		Faction3Bits = 283,
		[MappedEnum(typeof(FactionBits))]
		Hatred1Bits = 284,
		[MappedEnum(typeof(FactionBits))]
		Hatred2Bits = 285,
		[MappedEnum(typeof(FactionBits))]
		Hatred3Bits = 286,
		[SendOnCreate]
		SocietyRankCelhan = 287,
		[SendOnCreate]
		SocietyRankEldweb = 288,
		[SendOnCreate]
		SocietyRankRadblo = 289,
		HearLocalSignals = 290,
		HearLocalSignalsRadius = 291,
		Cleaving = 292,
		[SendOnCreate]
		AugmentationSpecializeGearcraft = 293,
		[SendOnCreate]
		AugmentationInfusedCreatureMagic = 294,
		[SendOnCreate]
		AugmentationInfusedItemMagic = 295,
		[SendOnCreate]
		AugmentationInfusedLifeMagic = 296,
		[SendOnCreate]
		AugmentationInfusedWarMagic = 297,
		[SendOnCreate]
		AugmentationCriticalExpertise = 298,
		[SendOnCreate]
		AugmentationCriticalPower = 299,
		[SendOnCreate]
		AugmentationSkilledMelee = 300,
		[SendOnCreate]
		AugmentationSkilledMissile = 301,
		[SendOnCreate]
		AugmentationSkilledMagic = 302,
		[MappedEnum(typeof(ImbuedEffectType))]
		ImbuedEffect2 = 303,
		[MappedEnum(typeof(ImbuedEffectType))]
		ImbuedEffect3 = 304,
		[MappedEnum(typeof(ImbuedEffectType))]
		ImbuedEffect4 = 305,
		[MappedEnum(typeof(ImbuedEffectType))]
		ImbuedEffect5 = 306,
		[SendOnCreate]
		DamageRating = 307,
		[SendOnCreate]
		DamageResistRating = 308,
		[SendOnCreate]
		AugmentationDamageBonus = 309,
		[SendOnCreate]
		AugmentationDamageReduction = 310,
		ImbueStackingBits = 311,
		[SendOnCreate]
		HealOverTime = 312,
		[SendOnCreate]
		CritRating = 313,
		[SendOnCreate]
		CritDamageRating = 314,
		[SendOnCreate]
		CritResistRating = 315,
		[SendOnCreate]
		CritDamageResistRating = 316,
		[SendOnCreate]
		HealingResistRating = 317,
		[SendOnCreate]
		DamageOverTime = 318,
		ItemMaxLevel = 319,
		[MappedEnum(typeof(ItemXpStyle))]
		ItemXpStyle = 320,
		EquipmentSetExtra = 321,
		[SendOnCreate]
		[MappedEnum(typeof(AetheriaBitfield))]
		AetheriaBitfield = 322,
		[SendOnCreate]
		HealingBoostRating = 323,
		[MappedEnum(typeof(HeritageGroup))]
		HeritageSpecificArmor = 324,
		AlternateRacialSkills = 325,
		[SendOnCreate]
		AugmentationJackOfAllTrades = 326,
		[SendOnCreate]
		AugmentationResistanceNether = 327,
		[SendOnCreate]
		AugmentationInfusedVoidMagic = 328,
		[SendOnCreate]
		WeaknessRating = 329,
		[SendOnCreate]
		NetherOverTime = 330,
		[SendOnCreate]
		NetherResistRating = 331,
		LuminanceAward = 332,
		[SendOnCreate]
		LumAugDamageRating = 333,
		[SendOnCreate]
		LumAugDamageReductionRating = 334,
		[SendOnCreate]
		LumAugCritDamageRating = 335,
		[SendOnCreate]
		LumAugCritReductionRating = 336,
		[SendOnCreate]
		LumAugSurgeEffectRating = 337,
		[SendOnCreate]
		LumAugSurgeChanceRating = 338,
		[SendOnCreate]
		LumAugItemManaUsage = 339,
		[SendOnCreate]
		LumAugItemManaGain = 340,
		[SendOnCreate]
		LumAugVitality = 341,
		[SendOnCreate]
		LumAugHealingRating = 342,
		[SendOnCreate]
		LumAugSkilledCraft = 343,
		[SendOnCreate]
		LumAugSkilledSpec = 344,
		[SendOnCreate]
		LumAugNoDestroyCraft = 345,
		RestrictInteraction = 346,
		OlthoiLootTimestamp = 347,
		OlthoiLootStep = 348,
		[MappedEnum(typeof(ContractId))]
		UseCreatesContractId = 349,
		[SendOnCreate]
		DotResistRating = 350,
		[SendOnCreate]
		LifeResistRating = 351,
		CloakWeaveProc = 352,
		[MappedEnum(typeof(WeaponType))]
		WeaponType = 353,
		[SendOnCreate]
		MeleeMastery = 354,
		[SendOnCreate]
		RangedMastery = 355,
		SneakAttackRating = 356,
		RecklessnessRating = 357,
		DeceptionRating = 358,
		CombatPetRange = 359,
		[SendOnCreate]
		WeaponAuraDamage = 360,
		[SendOnCreate]
		WeaponAuraSpeed = 361,
		[SendOnCreate]
		[MappedEnum(typeof(SummoningMastery))]
		SummoningMastery = 362,
		HeartbeatLifespan = 363,
		UseLevelRequirement = 364,
		[SendOnCreate]
		LumAugAllSkills = 365,
		[MappedEnum(typeof(SkillId))]
		UseRequiresSkill = 366,
		UseRequiresSkillLevel = 367,
		[MappedEnum(typeof(SkillId))]
		UseRequiresSkillSpec = 368,
		UseRequiresLevel = 369,
		[SendOnCreate]
		GearDamage = 370,
		[SendOnCreate]
		GearDamageResist = 371,
		[SendOnCreate]
		GearCrit = 372,
		[SendOnCreate]
		GearCritResist = 373,
		[SendOnCreate]
		GearCritDamage = 374,
		[SendOnCreate]
		GearCritDamageResist = 375,
		[SendOnCreate]
		GearHealingBoost = 376,
		[SendOnCreate]
		GearNetherResist = 377,
		[SendOnCreate]
		GearLifeResist = 378,
		[SendOnCreate]
		GearMaxHealth = 379,
		Unknown380 = 380,
		[SendOnCreate]
		PKDamageRating = 381,
		[SendOnCreate]
		PKDamageResistRating = 382,
		[SendOnCreate]
		GearPKDamageRating = 383,
		[SendOnCreate]
		GearPKDamageResistRating = 384,
		Unknown385 = 385,
		[SendOnCreate]
		Overpower = 386,
		[SendOnCreate]
		OverpowerResist = 387,
		[SendOnCreate]
		GearOverpower = 388,
		[SendOnCreate]
		GearOverpowerResist = 389,
		[SendOnCreate]
		Enlightenment = 390,
	}
}
